vpath %.cu src/
vpath %.h inc/

#change to match your cuda path
CUDA_PATH = /usr/local/cuda-11.1
COMP_INCLUDES= -I inc/ -I /usr/local/cuda-11.1/samples/common/inc
NVCC = $(CUDA_PATH)/bin/nvcc $(COMP_INCLUDES) 


#this parameter depends on the generation of your GPU, check the wikipedia page for CUDA gpus to determine your COMPUTE VERSION and remove the period for this. For example version 6.1 becomes 61
SM = 61
GENCODE_FLAGS = -gencode arch=compute_$(SM),code=compute_$(SM) 

LIBRARIES = -lcufft -lcublas -lcudart -lcusolver

MAIN = chiSquareFitting
SOURCES = general_functions.cu gpu_functions.cu filters.cu params.cu waves.cu
INCLUDES = $(SOURCES:.cu=.h) 
OBJECTS = $(SOURCES:.cu=.o) $(MAIN).o
BUILD = build/
EXECS =

#Building Code Here
all: $(MAIN)

$(MAIN): $(OBJECTS)
	$(NVCC) $(SAMPLES_PATH) $(GENCODE_FLAGS) -o $(EXECS)$@ $(addprefix $(BUILD),$(OBJECTS)) $(LIBRARIES) 

$(MAIN).o : $(MAIN).cu $(INCLUDES)
	$(NVCC) $(SAMPLES_PATH) $(GENCODE_FLAGS) -c $< -o $(BUILD)$@

%.o : %.cu %.h
	$(NVCC) $(SAMPLES_PATH) $(GENCODE_FLAGS) -c $< -o $(BUILD)$@ 

$(shell mkdir -p $(BUILD) $(EXECS))

clean :
	rm -f *.o *~ $(MAIN) build/*.o

