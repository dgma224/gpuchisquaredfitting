'''
This script is used to create the filter file that will be imported by
chiSquareFitting for analysis.


IMPORTANT NOTES!!
Any functions representing pulses must be put first in the filters list
All basis functions must have the same length
Any function representing a pulse should have its 't0' halfway through it
	For example a fit function of 800 long should have the pulse begin at 500
'''


'''
Fake Pulse generation script from Aaron Sprow
'''
import numpy as np
import struct

def makeVectorString(vector):
	string = ''
	for i in range(len(vector)):
		string+=str(vector[i])+' '
	string+='\n'
	return string

def makeMatrixString(matrix):
	string = ''
	for i in range(matrix.shape[0]):
		for j in range(matrix.shape[1]):
			string+=str(matrix[i,j])+' '
		string+='\n'
	return string

def fakeHeader(eventID, length):
	out = [True, eventID, 0, 0, eventID, length]
	return(struct.pack('<?iiiQi', *out))

def waveformOutput(waveform, shortString):
	waveform = waveform.astype(np.short)
	return waveform.tostring()

def makeTestDataFile(filename, purefilters, numWaves = 1, noiseAmp = 0):
	outputFile = open(filename,'wb')
	length = len(purefilters[0])
	shortString = str(length)+'h'
	for filt in range(len(purefilters)):
		for i in range(numWaves):
			noise = np.zeros(length)
			if noiseAmp != 0:
				noise = np.random.normal(0, scale=noiseAmp, size=length)
			wave = np.add(purefilters[filt], noise)
			outputFile.write(fakeHeader(filt, length))
			outputFile.write(waveformOutput(wave, shortString))
			if i == 0:
				np.savetxt('exampleWaveform.txt', wave)
	outputFile.close()
	return

def calculatePseudoinverseATWA(filters, weight = None):
	length = filters.shape[0]
	if weight is None: #for no weighting just use the precanned functions and whatnot
		inverseFilter = np.linalg.pinv(filters)
		ATWA = np.matmul(np.transpose(filters), filters)
		return inverseFilter[:,:], ATWA[:,:]
	else: #in the case of weighting, check the dimensionality and prep accordingly
		W = np.zeros((length, length))
		if weight.ndim==1:
			#in this case have to do the weird formula to do things
			W = np.zeros((length, length))
			for i in range(length):
				W[i,i] = weight[i]
		elif weight.ndim==2:
			W = weight[:,:]
		else:
			print('not an option')
			return [], []
		ATW = np.matmul(np.transpose(filters), W)
		ATWA = np.matmul(ATW, filters)
		ATWAInv = np.linalg.inv(ATWA)
		inverse = np.matmul(ATWAInv, ATW)
		return np.copy(inverse[:,:]), np.copy(ATWA[:,:])
  return

def calculateProjectionOperator(ideal, baselines, weight = None):
	matrix = [shape]
	for shape in baselines:
		matrix.append(shape)
	matrix = np.array(matrix)
	matrix = np.transpose(matrix)
	pseudoInverse = calculatePseudoinverseATWA(matrix, weight = weight)[0]
	projection = np.matmul(matrix, pseudoInverse)
	perpProjection = np.identity(len(matrix)) - projection
	if weight is None:
		return np.matmul(np.transpose(perpProjection), perpProjection)
	else:
		W = np.zeros((len(matrix), len(matrix)))
		if weight.ndim == 1:
			for i in range(len(matrix)):
				W[i,i] = weight[i]
		elif weight.ndim==2:
			W = weight[:,:]
		temp = np.matmul(W, perpProjection)
		return np.matmul(np.transpose(perpProjection), temp)

def prepareFilters(filename, idealPulses, baselines, scales, t0 = None, t0weight=None, psdWeight=None):
	length = len(idealPulses[0])
	numIdeal = len(idealPulses)
	numFitFunctions = len(idealPulses) + len(baselines)
	fitFunctionLength = len(idealPulses[0])
	if t0 == None:
		t0 = int(length/2)
	fullMatrix = []
	for shape in idealPulses:
		fullMatrix.append(shape)
	for shape in baselines:
		fullMatrix.append(shape)
	fullMatrix = np.array(fullMatrix)
	transMatrix = np.transpose(fullMatrix)
	inverseFilter, ATWA = calculatePseudoinverseATWA(transFitlers, weight = t0weight)
	#calculate the convolution filters
	convolutionFilters = np.zeros(inverseFilter.shape)
	for basis in range(numFitFunctions):
		convolutionFilters[basis,:] = np.flip(inverseFilter[basis,:])
	#now go through and determine the projection operators
	projectionOperators = []
	for ideal in idealPulses:
		projectionOperators.append(calculateProjectionOperator(ideal, baselines, weight = psdWeight))
	#with everything prepared, now open the output file
	outputFile = open(filename, 'w')
	#output initial information
	outputFile.write(str(numFitFunctions)+' '+str(fitFunctionLength)+' '+str(t0)+' '+str(numIdeal)+'\n')
	#output the basis functions
	outputFile.write(makeMatrixString(fullMatrix))
	#output the pseudoinverse filters
	outputFile.write(makeMatrixString(inverseFilter))
	#output the ATWA thing
	outputFile.write(makeMatrixString(ATWA))
	#handle the weights
	if t0weight is None:
		outputFile.write('0\n')
	else:
		if t0weight.ndim == 1:
			outputFile.write('1\n')
			outputFile.write(makeVectorString(t0weight))
		elif t0weight.ndim == 2:
			outputFile.write('2\n')
			outputFile.write(makeMatrixString(t0weight))
	#output the energy scales
	outputFile.write(makeVectorString(scales))
	#now output the projection operators in the case that psd is needed
	if numIdeal > 1:
		for op in projectionOperators:
			outputFile.write(makeMatrixString(op))
	outputFile.close()
	return
