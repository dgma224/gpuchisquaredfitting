/*
debug.h
This header file contains the debug functions
*/

#ifndef __DEBUG_H_INCLUDED__
#define __DEBUG_H_INCLUDED__

#include <iostream>
#include <string>
#include <limits>
#include <iomanip>

typedef float2 Complex;
namespace debug {
/*
std::ostream& operator<<(std::ostream& os, const Complex &array) {
  os << array.x << " " << array.y;
  return os;
}*/

template <typename T>
void arrayWrite (const T *data, int len, std::string filename) {
  std::ofstream fout (filename);
	int precision = std::numeric_limits<T>::digits10 + 1;
  for (int i = 0; i < len; i++)
    fout << std::fixed << std::setprecision(precision) << data[i] << std::endl;
  fout.close();
  std::cout <<filename <<" has been printed" <<std::endl;
}

template <typename T>
void d_arrayWrite (const T *d_data, int len, std::string filename) {
  T *data;
  checkCudaErrors(cudaMallocHost(&data, sizeof(T) * len));
  checkCudaErrors(cudaMemcpy(data, d_data, sizeof(T) * len,
        cudaMemcpyDeviceToHost));
  std::ofstream fout (filename);
	int precision = std::numeric_limits<T>::digits10 + 1;
	for (int i = 0; i < len; i++)
    fout << std::fixed << std::setprecision(precision)<< data[i] << std::endl;
  fout.close();
  std::cout <<filename <<" has been printed" <<std::endl;
  cudaFreeHost(data);
}

template <typename T>
void d_matrixWrite(const T *d_data, int xdim, int ydim, std::string filename){
  T *data;
  checkCudaErrors(cudaMallocHost(&data, sizeof(T) * xdim * ydim));
  checkCudaErrors(cudaMemcpy(data, d_data, sizeof(T) * xdim * ydim, cudaMemcpyDeviceToHost));
  std::ofstream fout(filename);
  
	int precision = std::numeric_limits<T>::digits10 + 1;
	for (int i = 0; i < xdim; i++){
    for (int j = 0; j < ydim; j++){
      fout << std::fixed<< std::setprecision(precision)<<data[j + i * ydim]<< " ";
    }
    fout<<std::endl;
  }
  fout.close();
  std::cout<<filename<<" has been printed"<<std::endl;
  cudaFreeHost(data);
}




}//namespace debug


#endif
