//filters.h
#ifndef __FILTERS_H_INCLUDED__
#define __FILTERS_H_INCLUDED__

#include <iostream>
#include <fstream>
#include <string>
#include "params.h"
#include <cufft.h>
#include <cublas_v2.h>
#include <cuda_runtime.h>
#include <helper_functions.h>
#include <helper_cuda.h>

typedef float2 Complex;

namespace gpuAnalysis {

class FILTERS {
/* This contains the storage for the various filters that
are used.
*/
	public:
	FILTERS(PARAMS &param);
	~FILTERS();
	// Methods used in construction of filters
	void getBasisFunctions(PARAMS &param);
	void trapFilterGen  (PARAMS &param);
	void fftFilters     (PARAMS &param);
	void calculatePseudoInverse(double *, double **, int, int);
	void defChiSquareFit(PARAMS &param);
	void boxcarFilterGen(PARAMS &param);
	void prepProjectionOperators(PARAMS &param);

	//if weighting is used and what type
	int weighting = 0;//0 means no, 1 means only diagonal, 2 means off diagonals
	int quadFitLen = 0;//the length of the quadratic fitting
	int psdWeight = 0;
	//storage for the trapezoidal filter
	double *dotTrapFilter, *d_dotTrapFilter;

	double2 *d_fftTrapFilter;

	//pointers for storing fit matrix information
	double *fitFunctions, *d_fitFunctions;

	double *h_boxFilter, *d_boxFilter; //in the case that weighting option 1 is used
	double2 *d_fftBoxFilter;//fft'd boxcar filter	

	//storage for the weighting matrix if it's used
	double *h_weightMatrix, *d_weightMatrix;

	double *inverseFitFunctions, *d_inverseFitFunctions;
	double2 *d_fftInverseFitFunctions;

	double *fitMatrixSquared, *d_fitMatrixSquared;

	double *enerScales, *d_enerScales;
	//want to do this inversion in double precision since we just do it once so that's a fine method to use
	double *d_quadFitMatrix;

	//PSD variables
	int psdWeighting = 0;
	double *psdYTYWeight, *d_psdYTYWeight;
	double **psdATA, **d_psdATA;
	double **psdInverses;
	double **d_psdInverses;

	//now various integers that give me information about the fit functions
	int numIdealPulses, numBaselineFunctions, numFitFunctions, fitFunctionLength, pretrigger;
	int fitFunctionPaddedLength;
	int t0offset; //how far into the fit functions the t0 value is
	
	//various sizes
	size_t  filtsSize, filtsComplexSize, basisSize; // Sizes of filters   

	//the fft plans that are used
	cufftHandle planSingleFilter;
	cufftHandle planSingleFilterDouble;
	cufftHandle planBasisFunctions;
};

}  // Namespace gpuAnalysis
#endif
