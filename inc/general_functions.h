//general functions .h
#ifndef __GENERAL_FUNCTIONS_H_INCLUDED__
#define __GENERAL_FUNCTIONS_H_INCLUDED__

#include "gpu_functions.h"
#include "waves.h"
#include "debug.h"
#include <stdio.h>

#include "params.h"

namespace gpuAnalysis{

struct WAVEFORM{
	bool result;
	int eventID;
	int board;
	int channel;
	unsigned long timestamp;
	unsigned long req;
	int length;
	int complength;
	short firstval;
};

WAVEFORM readHead(FILE*, int);
WAVEFORM readHeadComp(FILE*);

void readWave(FILE*, short*, int);
void readWaveComp(FILE*, unsigned int *, unsigned short);
void writeWave(FILE*, short*, int);
void writeHead(FILE*, WAVEFORM, int);

int calcNumBlocks(int, int);

void readBatch(FILE*, WAVES&, WAVEFORM*, int, PARAMS);
void writeBatch(FILE*, short*, WAVEFORM*, int, PARAMS);
void writeResult(FILE*, float2*, WAVEFORM*, int, PARAMS);

void fileSetUp(FILE*&, FILE*&, int, PARAMS&, FILTERS&);

void sendToGPU(WAVES&, PARAMS&);
void wavesProcessing(WAVES&, PARAMS&);
void applyFilters(WAVES&, FILTERS&, PARAMS&);
void calculateYTWY(WAVES&, FILTERS&, PARAMS&);
void calculatexTATAx(WAVES&, FILTERS&, PARAMS&);
void findChiSquared(WAVES&, FILTERS&, PARAMS&);
void findMinChiSquared(WAVES&, FILTERS&, PARAMS&);
void findEnergy(WAVES&, FILTERS&, PARAMS&);
void findPulseShapes(WAVES&, FILTERS&, PARAMS&);
void getResultsFromGPU(WAVES&, FILTERS&, PARAMS&);

std::string getHeaderString(WAVEFORM, int);
void writeResults(FILE*, WAVEFORM*, WAVES&, FILTERS&, int, PARAMS&);

}
#endif
