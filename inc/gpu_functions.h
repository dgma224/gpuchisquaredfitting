/*
gpu_functions.h
This header file contains the definitions of the various GPU functions that are used.
Many of these are overloaded to support using 64 bit precision as well
*/

#ifndef __GPU_FUNCTIONS_H_INCLUDED__
#define __GPU_FUNCTIONS_H_INCLUDED__

typedef float2 Complex;
typedef double2 ComplexD;

namespace gpuAnalysis {

__global__ void applyFitFunctionFilters(ComplexD *, ComplexD *, ComplexD *, int, int, int);

__global__ void squareWaveforms(double*, double*, int);

__global__ void multiplication(double*, double*, double*, int, int);
__global__ void multiplication(ComplexD *, ComplexD *, int, int);

__global__ void calculateSumIdealFitParameters(double*, double*, int, int, int, int, int);

__global__ void findMaxLocIdealFitParameter(int*, double*, int, int, int, int, int, int);

__global__ void grabRelevantWaveformPieces(double*, double*, int*, int, int, int, int, int);

__global__ void grabRelevantWaveformPiecesForFit(double*, double*, float*, int, int, int, int);

__global__ void grabRelevantElementsYTY(double*, double*, int*, int, int, int, int);

__global__ void grabRelevantElementsX(double*, double*, int*, int, int, int, int, int);

__global__ void grabRelevantElementsChiSquared(double*, int *, double*, float*, int);

__global__ void calculateChiSquared(double*, double*, double*, int);

__global__ void noSearchGrabValues(double*, float*, double*, double*, int*, double*, int, int, int, int, int);

__global__ void localChiSquaredMinSearch(double*, float*, double*, double*, int*, double*, int, int, int, int, int, int, int);

__global__ void chiMinSearchAndGrab(double*, double*, float*, int, int, int, int);

__global__ void calculateChiFitMin(double*, float*, double*, int*, double*, double*, int, int, int, int, int, int, int);

__global__ void grabResultFitParameters(double*, double*, float*, int, int, int); 

__global__ void shiftMinChiSquaredValue(double*, double*, int, int);

__global__ void generateIdealWaveforms(double*, double*, double*, int, int, int, int);

__global__ void gpuPSD(int*, float*, double*, int, int);

__global__ void	addAllBasisFunctions(float *, float*, float*, int, int, int, int); 
__global__ void determineResidualsSquared(float*, float*, float*, int, int, int);
__global__ void findSigmaAndNormalizeChiSquared(float*, float*, int, int, int, int);

//type conversions
__global__ void typeConversion(short*, float*, int);
__global__ void typeConversion(short*, double*, int);
__global__ void typeConversion(double*, float*, int);

//converting from 14bit to 16 bit functions
__global__ void andFunction(short *data, int len);
__global__ void andFunctionPad(short *, int, int, int);
__global__ void andFunctionShortToFloat(const short *inData, float *outData, 
		int len);
__global__ void averagesFind(const short *data, float *avgs, int np, int numWaves, int offset);

//generator of the boxcar filter
__global__ void defineBoxcarFilter(float *, int, int);

//various baseline shift methods
__global__ void baselineShift(short *data, const int *avgs, int np, int len);
__global__ void baselineShift(float *data, const int *avgs, int padnp, int np, int len);
__global__ void baselineShiftShortToFloat(const short *inData, float *outData, 
		const float *avgs, int padnp, int np, int len);

//energy extraction function
__global__ void knownEnergyT0(const double*, float*, int, int, int);
__global__ void energyT0(const double *, Complex *, int, int, int, int, int, float);
__global__ void energyT0(const double *, float *, int, int, int, int, int, float);

//decompression
__global__ void gpuDecompression(short *, const unsigned int*, const int*, const short*, int, int, int, int);

} // namespace gpuAnalysis

#endif
