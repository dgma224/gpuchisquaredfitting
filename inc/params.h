//params.h
#ifndef __PARAMS_H_INCLUDED__
#define __PARAMS_H_INCLUDED__

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <stdlib.h>

namespace gpuAnalysis {

//const int TRAP = 0, CUSP = 1, CUST_FFTED = 2, CUST_RAW = 3,  // Fitler type
//			 FILTER_CONVOLUTION = 0, WAVE_FIT = 1;  // Analysis method type

//Parameter File Inputs for Filters
struct PARAMS {
	/* David's struct for parameters that are read in to be used, updated
   */
   // Default values set from Dave's orig specs
	int batchSize   = 2048,  // Number of waveforms per batch processed
			top         = 0,     // Of trap filter
 			rise        = 0,     // Of trap filter
			wavelength  = 3500,  // Number of data points in waveform
			searchRange = 0, 
			quadFitLen = 0,
			searchLen = 0,
			padWavelength = 7000,  // Number of points in the padded length for the fft of the basis functions and the waveform
		 	paddedFFTLength     = 0,     //how long the complex data series will be for the fft of the basis functions and the waveform
			numBatches = 0,      // how many batches of waveforms are processed
			lastBatch = 0,       // if there is one extra batch to do or not
			leftover = 0,        // how many waveforms are left after the batches
			fileFormat = 0,			 // 0 for nab, 1 for ca45
			gpusToUse = -1;			 // the number of GPUs to use. If -1, use all of them that are available
	float  tau         = 0,     // Of trap filter
			percentThreshold=0.8f,
			trapScaleFloat = 1.0f;
	double trapScaleDouble = 1.0;
	int trapLen = 0;
	double fftNormalization= 1.0f;   // For correct scaling on multiplication function
	bool isCompressed = false;//determines if the input files are compressed or not
	int maxCompLen = 0; //value for the largest size of waveform that is compressed
	int numWaves = 0;   //number of waveforms in the file
	std::vector<std::string> datafiles;
	std::string filterFile;	
	//here are the values that used to be stored in the vals struct, they've been moved here for simplicity
	size_t shortSize, shortSizeBatch,
				intSize, intSizeBatch, intSizeComp,
				floatSize, floatSizeBatch,
				doubleSize, doubleSizeBatch,
				complexSize, complexHalfSizeBatch, complexSizeBatch,
				complexDoubleSize, complexDoubleHalfSizeBatch, complexDoubleSizeBatch;
	int mulThreads, mulBlocks,
			andThreads, andBlocks,
			baseThreads, baseBlocks,
			sumThreads, sumBlocks,	
			subThreads, subBlocks,
			searchThreads, searchBlocks;
	// Constructor, replaced "fillParams". "defaultParam" replaced with initializations
	PARAMS();
  PARAMS(std::string data_location) : PARAMS() { getDataFiles(data_location); }
  PARAMS(const PARAMS&);
	void getDataFiles(std::string data_location); // Invoked by alt constructor 
	void getFileInformation(std::string filename);
	void determineSizes();
};
}  // Namespace gpuAnalysis
#endif
