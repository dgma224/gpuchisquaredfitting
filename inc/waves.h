//waves.h
#ifndef __WAVES_H_INCLUDED__
#define __WAVES_H_INCLUDED__

#include "filters.h"
namespace gpuAnalysis{
class WAVES {
	public:
		int weighting, numIdealPulses, psdWeight;
		short   *waveforms, *d_waveforms;   // Waveforms storage	
		//waveform storage
		double *d_doubleWaveforms;

		//storage forthe waveform after being FFTd and temporary storage
		double2 *d_complexWaveforms, *d_complexWaveformsCopy;

		//now for the fit parameter storage
		double *d_fitParameters;

		//storage of the sum of the fit parameters
		double *d_sumFitParams;

		//fft version of the fit parameters
		double2 *d_fftFitParameters;

		//location of the max fit parameters
		int *d_maxFitLocs;

		//relevant fit parameters
		double *d_relevantFitParams;

		//relevant sections of the waveforms for the yTy calculation when weighting == 2
		double *d_relevantWaveformPieces;
		
		//storage for the temporary values
		double *d_temporaryYTYValues;
		
		//relevant YTY values
		double *d_relevantYTY;

		//temporary storage for xATAx values
		double *d_tempxATAx;

		//storage for resutls of xATAx values
		double *d_xATAx;

		//waveform squared information
		double *d_waveformSquared;

		//waveform squared FFT part
		double2 *d_fftWaveformSquared;

		//chi squared values and fit region
		double *d_chiSquared, *d_fitChiSquared;

		//storage for the quadratic chi squared fit locations so the true location can be determined
		int *d_quadFitLocs;

		//chi squared fit parameters
		double *d_chiSquaredFitParams;

		//storage for the fit results
		double *fitResults, *d_fitResults;

		//storage for the energy results
		float *trapResults, *d_trapResults;
		double *d_trapResultsDouble;
		//storage for chi squared results
		double *minChiSquared, *d_minChiSquared;
		float *minChiSquaredLoc, *d_minChiSquaredLoc;

		//storage for the PSD operations
		double *d_relevantFitPieces;
		double *d_psdTempYTY;
		double *d_psdYTY;
		double *d_psdTempATA;
		double *d_psdATA;
		double *d_PSDChis;
		double *d_psdParams;

		int *psdPredictions, *d_psdPredictions;	
		float *percents, *d_percents;

		//cuda fft information
		cufftHandle planWavesForward, planFitParamsInverse, planSquaredInverse;
		cudaStream_t stream;

		//cublas information
		cublasStatus_t cublasStatus;
		cublasHandle_t cublasHandle;

		//compressed waveform data
		short *firsts, *d_firsts;
		unsigned int *compwaves, *d_compwaves;
		int *complengths, *d_complengths;

		WAVES(PARAMS&, FILTERS&);
		~WAVES();
};

}

#endif
