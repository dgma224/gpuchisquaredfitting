//David Mathews

/*
This code is a real time chi-squared fitting function for data from the
Nab experiment.
*/

//general c/c++ inclusions
#include <stdlib.h>//standard
#include <stdio.h>//standard
#include <string.h> //used for file handling
#include <iostream> //used for input/output
#include <chrono> //used for timing information
#include <thread>//used for threading
#include <utility> //used for passing references to threads

//my file inclusions
#include "params.h"
#include "filters.h"
#include "waves.h"
#include "gpu_functions.h"
#include "general_functions.h"
#include "debug.h"

//cuda inclusions
#include <cuda_runtime.h>
#include <cufft.h>
#include <helper_functions.h>
#include <helper_cuda.h>
#include <cublas_v2.h>

typedef float2 Complex;
typedef double2 ComplexD;

using namespace gpuAnalysis;

void chiSquaredMinimization(PARAMS&, FILTERS&, int);
void distributeWork(PARAMS&);
void singleThreadedAnalysis(PARAMS&);
void MultiThreadedAnalysis(PARAMS&, int, std::vector<int>);
void threadedAnalysis(PARAMS&, int, std::vector<int>);

int main(int argc, char *argv[]){
	//first get the parameters and whatnot
	PARAMS param("dataloc.dat");
	//now iterate over each file
	int numFiles = param.datafiles.size();
	if(numFiles == 0){
		std::cout<<"No Files in dataloc.dat: code exiting"<<std::endl;
		return 0;
	}
	param.getFileInformation(param.datafiles.at(0));
	distributeWork(param);
	return 0;
}

//this function determines how to distribute the work based on the input information
void distributeWork(PARAMS& param){
	std::vector<std::vector<int>> filesToProcess;
	if(param.gpusToUse != -1){//in the case that we want to use a single GPU
		int numGPUs;
		cudaGetDeviceCount(&numGPUs);
		if(param.gpusToUse >= numGPUs){
			std::cerr<<"requesting a GPU that doesn't exist"<<std::endl;
			exit(1);
		}
		filesToProcess.resize(1);
		for(int i = 0; i < param.datafiles.size(); i++){
			filesToProcess[0].push_back(i);
		}
		std::thread t1(threadedAnalysis, std::ref(param), param.gpusToUse, filesToProcess[0]);
		t1.join();
	}
	else{
		//in this case we want to use multiple GPUs so distribute accordingly
		checkCudaErrors(cudaGetDeviceCount(&param.gpusToUse));
		filesToProcess.resize(param.gpusToUse);
		int currGPU = 0;
		for(int i = 0; i < param.datafiles.size(); i++){
			filesToProcess[currGPU].push_back(i);
			currGPU = (currGPU + 1) % param.gpusToUse;
		}
		std::vector<std::thread> threads;
		for(int i = 0; i < filesToProcess.size(); i++){
			threads.push_back(std::thread(threadedAnalysis, std::ref(param), i, filesToProcess[i]));
		}
		for(int i = 0; i < filesToProcess.size(); i++){
			threads[i].join();
		}
	}
}

//this function handles the calling of the chiSquaredMinimizationFunction for each thread
void threadedAnalysis(PARAMS &param, int gpuID, std::vector<int> toProcess){
	//first assign the GPU that will be used
	cudaSetDevice(gpuID);
	//now go through and load in the filter information
	FILTERS filters(param);
	for(int file = 0; file < toProcess.size(); file++){
		chiSquaredMinimization(param, filters, toProcess[file]);
	}
}

void chiSquaredMinimization(PARAMS &param, FILTERS& filters, int fileNum){
	//start the timer
	auto start = std::chrono::system_clock::now();
	//get the file information
	param.getFileInformation(param.datafiles.at(fileNum));
	//now setup the input and output files
	FILE *fin, *fout;
	fileSetUp(fin, fout, fileNum, param, filters);
	//setting up the file
	//allocate the header storage
	WAVEFORM *headers = (WAVEFORM*)malloc(sizeof(WAVEFORM) * param.batchSize * 2);
	//check the precision type to be used in the parameter file
	WAVES waves1(param, filters);
	WAVES waves2(param, filters);
	//std::cout<<"set up processing"<<std::endl;	
	//make sure all GPU work at this point is complete and executed properly
	checkCudaErrors(cudaDeviceSynchronize());
	//now start the main analysis loop
	for (int batch = 0; batch < (int(param.numBatches/2)); batch += 1){
		//read in the first batch
		readBatch(fin, waves1, headers, param.batchSize, param);
		//std::cout<<"\t read in data"<<std::endl;
		//send to the GPU
		sendToGPU(waves1, param);
		//std::cout<<"\t sent to GPU"<<std::endl;
		//do setup work on waveforms, de-compress if need be, apply 14->16 bit conversion, then do FFT
		wavesProcessing(waves1, param);
		//std::cout<<"\t processed waves"<<std::endl;
		//now apply the different FFT filters
		applyFilters(waves1, filters, param);
		//std::cout<<"\t applied filters"<<std::endl;
		//with the filters applied, the fit parameters have been found
		//now determine the two parts of the chi squared equation
		calculateYTWY(waves1, filters, param);
		//std::cout<<"\t calculated YTWY"<<std::endl;
		calculatexTATAx(waves1, filters, param);
		//std::cout<<"\t calculated xTATAx"<<std::endl;
		//now find the chi squared value
		findChiSquared(waves1, filters, param);	
		//std::cout<<"\t found chi squared"<<std::endl;
		//now the chi squared values as a function of time are known
		//we need to find the minimum chi values now
		findMinChiSquared(waves1, filters, param);
		//std::cout<<"\t minimized chi squared"<<std::endl;
		//now the location of the minimum chi squared is known (t0)
		//and its value is known but hasn't been normalized yet
		//now find the trapezoidal energy and normalize the chi squared value
		findEnergy(waves1, filters, param);
		//now determine the pulse shapes
		findPulseShapes(waves1, filters, param);
		//std::cout<<"\t found energy and normalized"<<std::endl;
		//at this point everything has been calculated
		//start the process on the second stream
		//std::cout<<"starting batch 2"<<std::endl;
		readBatch(fin, waves2, &headers[param.batchSize], param.batchSize, param);	
		sendToGPU(waves2, param);
		wavesProcessing(waves2, param);
		applyFilters(waves2, filters, param);
		calculateYTWY(waves2, filters, param);
		calculatexTATAx(waves2, filters, param);
		findChiSquared(waves2, filters, param);
		findMinChiSquared(waves2, filters, param);
		findEnergy(waves2, filters, param);
		findPulseShapes(waves2, filters, param);
		//now pull back the results from stream1
		//std::cout<<"finished batch 2"<<std::endl;	
		getResultsFromGPU(waves1,filters, param);
		//std::cout<<"grabbed results batch 1"<<std::endl;			
		//output the results to the file in the .csv format	
		writeResults(fout, headers, waves1, filters, param.batchSize, param);
		//std::cout<<"wrote results batch 1"<<std::endl;
		//pull results from stream 2
		getResultsFromGPU(waves2, filters, param);
		//std::cout<<"grabbed results batch 2"<<std::endl;
		writeResults(fout, &headers[param.batchSize], waves2, filters, param.batchSize, param);
		//std::cout<<"\t batch "<<batch<<" completed"<<std::endl;
	}
	//now finish up the work load
	if(param.lastBatch){
		//do one normal batch
		readBatch(fin, waves1, headers, param.batchSize, param);
		sendToGPU(waves1, param);
		wavesProcessing(waves1, param);
		applyFilters(waves1, filters, param);
		calculateYTWY(waves1, filters, param);
		calculatexTATAx(waves1, filters, param);
		findChiSquared(waves1, filters, param);	
		findMinChiSquared(waves1, filters, param);
		findEnergy(waves1, filters, param);
		findPulseShapes(waves1, filters, param);
		getResultsFromGPU(waves1,filters, param);			
		writeResults(fout, headers, waves1, filters, param.batchSize, param);
	}
	//if there is anything after the last batch do it
	if(param.leftover){
		readBatch(fin, waves1, headers, param.leftover, param);
		sendToGPU(waves1, param);
		wavesProcessing(waves1, param);
		applyFilters(waves1, filters, param);
		calculateYTWY(waves1, filters, param);
		calculatexTATAx(waves1, filters, param);
		findChiSquared(waves1, filters, param);
		findMinChiSquared(waves1, filters, param);
		findEnergy(waves1, filters, param);
		findPulseShapes(waves1, filters, param);
		getResultsFromGPU(waves1,filters, param);			
		writeResults(fout, headers, waves1, filters, param.leftover, param);
	}

	//close the output files
	fclose(fin);
	fclose(fout);
	auto stop = std::chrono::system_clock::now();
	std::chrono::duration<double> t = stop - start;
  std::cout << param.datafiles.at(fileNum) << " : " << param.numWaves << " : "
         << t.count() << std::endl;
	free(headers);
}


