//filters.cu

#include "filters.h"
#include "general_functions.h"
#include "gpu_functions.h"
#include <cufft.h>
#include <cusolverDn.h>

namespace gpuAnalysis {

//------------------------------------
// 			 Constructor
//------------------------------------
FILTERS::FILTERS(PARAMS &param) {
	//first read in the basis functions and create those on GPU
	getBasisFunctions(param);
	checkCudaErrors(cudaDeviceSynchronize()); 
	//with this known, update the parameters to have the proper sizes
	param.determineSizes();
	//now go through and process these filters with the GPU

	//now generate the trapezoidal filter
	trapFilterGen(param);
	checkCudaErrors(cudaDeviceSynchronize()); 
	
	if(param.quadFitLen > 0){
		quadFitLen = param.quadFitLen;
		defChiSquareFit(param);
	}
	
	checkCudaErrors(cudaDeviceSynchronize());
	//do the fft of all of the filters
	fftFilters(param);
	checkCudaErrors(cudaDeviceSynchronize()); 
}

void FILTERS::getBasisFunctions(PARAMS &param){
	//first open the file to get the information from
	FILE *matrixFile = NULL;
	matrixFile = fopen(param.filterFile.c_str(), "rb");
	if(matrixFile != NULL){
		//with the file open, now parse the data
		//first number is the number of basis functions total
		fread(&numFitFunctions, sizeof(int), 1, matrixFile);
		fread(&fitFunctionLength, sizeof(int), 1, matrixFile);
		fread(&pretrigger, sizeof(int), 1, matrixFile);
		fread(&numIdealPulses, sizeof(int), 1, matrixFile);
		numBaselineFunctions = numFitFunctions - numIdealPulses;
		//knowing how long the basis functions are lets us determine the amount of padding needed for convolution reasons
		param.padWavelength = pow(2, ceil(log(param.wavelength+param.wavelength-1)/log(2)));
		param.paddedFFTLength = int(param.padWavelength / 2) + 1;//this is how long the fft of each one will be
		param.fftNormalization = 1.0/((double)param.padWavelength);	
		//for the fit functions, we only need to know the ones that are ideal pulses, the others won't be directly used except for the pseudoinverse
		fitFunctions = (double*)calloc(param.padWavelength * numFitFunctions, sizeof(double));
		for(int i = 0; i < numFitFunctions; i++){
			fread(&fitFunctions[i*param.padWavelength], sizeof(double), fitFunctionLength, matrixFile);
		}
		//move these to the GPU
		checkCudaErrors(cudaMalloc(&d_fitFunctions, sizeof(double) * param.padWavelength * numFitFunctions));	
		checkCudaErrors(cudaMemcpy(d_fitFunctions, fitFunctions, sizeof(double) * param.padWavelength * numFitFunctions, cudaMemcpyHostToDevice));
		//now read in the inverted fit functions
		inverseFitFunctions = (double*)calloc(param.padWavelength * numFitFunctions, sizeof(double));
		for(int i = 0; i < numFitFunctions; i++){
			fread(&inverseFitFunctions[i*param.padWavelength], sizeof(double), fitFunctionLength, matrixFile);
			for(int j = 0; j < fitFunctionLength; j++){
				inverseFitFunctions[j + i * param.padWavelength] *= param.fftNormalization;
			}
		}
		checkCudaErrors(cudaMalloc(&d_inverseFitFunctions, sizeof(double) * param.padWavelength * numFitFunctions));
		checkCudaErrors(cudaMemcpy(d_inverseFitFunctions, inverseFitFunctions, sizeof(double) * param.padWavelength * numFitFunctions, cudaMemcpyHostToDevice));
		//now read in the fitMatrixSquared
		//this matrix will be numFitFunctions x numFitFunctions
		fitMatrixSquared = (double*)malloc(numFitFunctions * numFitFunctions * sizeof(double));
		fread(fitMatrixSquared, sizeof(double), numFitFunctions * numFitFunctions, matrixFile);
		checkCudaErrors(cudaMalloc(&d_fitMatrixSquared, sizeof(double) * numFitFunctions * numFitFunctions));
		checkCudaErrors(cudaMemcpy(d_fitMatrixSquared, fitMatrixSquared, sizeof(double) * numFitFunctions * numFitFunctions, cudaMemcpyHostToDevice));
		
		//now check to see if the functions are weighted or not
		//so far everything read in is the same with or without weighting
		fread(&weighting, sizeof(int), 1, matrixFile);
		if(weighting == 0 || weighting == 1){//these are bundled together as during calculations they are identical
			if(weighting == 0){//define the boxcar filter
				h_boxFilter = (double*)calloc(param.padWavelength, sizeof(double));
				for(int i = 0; i < fitFunctionLength; i++){
					h_boxFilter[i] = 1.0/double(param.padWavelength);
				}
				
			}
			else if (weighting == 1){
				h_boxFilter = (double*)calloc(param.padWavelength, sizeof(double));
				fread(&h_boxFilter, sizeof(double), fitFunctionLength, matrixFile);
				for(int i = 0; i < fitFunctionLength; i++){
					h_boxFilter[i] = h_boxFilter[i]/double(param.padWavelength);
				}
			}
			//move things to the GPU
			checkCudaErrors(cudaMalloc(&d_boxFilter, sizeof(double) * param.padWavelength));
			checkCudaErrors(cudaMemcpy(d_boxFilter, h_boxFilter, sizeof(double) * param.padWavelength, cudaMemcpyHostToDevice));
		}
		else if(weighting == 2){
			h_weightMatrix = (double*)calloc(fitFunctionLength * fitFunctionLength, sizeof(double));
			fread(h_weightMatrix, sizeof(double), fitFunctionLength * fitFunctionLength, matrixFile);
			checkCudaErrors(cudaMalloc(&d_weightMatrix, sizeof(double) * fitFunctionLength * fitFunctionLength));
			checkCudaErrors(cudaMemcpy(d_weightMatrix, h_weightMatrix, sizeof(double) * fitFunctionLength * fitFunctionLength, cudaMemcpyHostToDevice));
		}
		else{
			printf("Weighting option not recognized\n");
			exit(0);
		}
		//with the weights read in, now read in the energy scale factors
		enerScales = (double*)calloc(numFitFunctions, sizeof(double));
		fread(enerScales, sizeof(double), numIdealPulses, matrixFile);
		checkCudaErrors(cudaMalloc(&d_enerScales, sizeof(double) * numFitFunctions));
		checkCudaErrors(cudaMemcpy(d_enerScales, enerScales, sizeof(double) * numFitFunctions, cudaMemcpyHostToDevice));
		//now read in the matrices for the PSD algorithm
		if(numIdealPulses > 1){
			fread(&psdWeight, sizeof(int), 1, matrixFile);
			psdInverses = (double**)malloc(sizeof(double*)*numIdealPulses);
			d_psdInverses = (double**)malloc(sizeof(double*)*numIdealPulses);
			psdATA = (double**)malloc(sizeof(double*)*numIdealPulses);
			d_psdATA = (double**)malloc(sizeof(double*)*numIdealPulses);
			int tempsize = numFitFunctions - numIdealPulses + 1;
			tempsize = tempsize * tempsize;
			if(psdWeight == 0){
				//do nothing
			}
			else if(psdWeight == 1){
				psdYTYWeight = (double*)malloc(sizeof(double)*fitFunctionLength);
				fread(psdYTYWeight, sizeof(double), fitFunctionLength, matrixFile);
				checkCudaErrors(cudaMalloc(&d_psdYTYWeight, sizeof(double) * fitFunctionLength));
				checkCudaErrors(cudaMemcpy(d_psdYTYWeight, psdYTYWeight, sizeof(double) * fitFunctionLength, cudaMemcpyHostToDevice));
			}
			else if(psdWeight == 2){
				psdYTYWeight = (double*)malloc(sizeof(double)*fitFunctionLength*fitFunctionLength);
				fread(psdYTYWeight, sizeof(double), fitFunctionLength*fitFunctionLength, matrixFile);
				checkCudaErrors(cudaMalloc(&d_psdYTYWeight, sizeof(double) * fitFunctionLength*fitFunctionLength));
				checkCudaErrors(cudaMemcpy(d_psdYTYWeight, psdYTYWeight, sizeof(double) * fitFunctionLength*fitFunctionLength, cudaMemcpyHostToDevice));
			}
			else{
				printf("unrecognized PSD weight: code exiting\n");
				exit(-1);
			}
			for(int i = 0; i < numIdealPulses; i++){
				//read in the pseudoinverses
				psdInverses[i] = (double*)malloc(sizeof(double)*fitFunctionLength*tempsize);
				fread(psdInverses[i], sizeof(double), fitFunctionLength * tempsize, matrixFile);
				checkCudaErrors(cudaMalloc(&d_psdInverses[i], sizeof(double)*fitFunctionLength*tempsize));
				checkCudaErrors(cudaMemcpy(d_psdInverses[i], psdInverses[i], sizeof(double)*fitFunctionLength*tempsize, cudaMemcpyHostToDevice));	
				//read in the ATWA matrix
				psdATA[i] = (double*)malloc(sizeof(double)*tempsize);
				fread(psdATA[i], sizeof(double), tempsize, matrixFile);
				checkCudaErrors(cudaMalloc(&d_psdATA[i], sizeof(double)*tempsize));
				checkCudaErrors(cudaMemcpy(d_psdATA[i], psdATA[i], sizeof(double) * tempsize, cudaMemcpyHostToDevice));
			}
		}
	}
	else{
		std::cout<<param.filterFile<<" : failed to open"<<std::endl;
		exit(1);
	}
}


void reverseArray(double *array, int start, int stop){
	while(start < stop){
		double temp = array[start];
		array[start] = array[stop];
		array[stop] = temp;
		start++;
		stop--;
	}
}
//---------------------------------
//         trapFilterGen
//---------------------------------
void FILTERS::trapFilterGen(PARAMS &param) {
	//this filter is always in single precision since otherwise it doesn't really matter
	//first allocate the filter
	dotTrapFilter = (double*)calloc(param.rise * 2 + param.top, sizeof(double));
	//remember to scale the filter properly, this is done
	double scale = 1.0/(param.tau * param.rise); 	
	for (int i = 0; i < param.rise; i++) {
		dotTrapFilter[i] = (param.tau + i)*scale;
		dotTrapFilter[i+param.rise+param.top] = (param.rise - param.tau - i)*scale;
	}
	for (int i = param.rise; i < param.rise + param.top; i++){
		dotTrapFilter[i] = param.rise * scale;
	}
	// Copy to Device
	reverseArray(dotTrapFilter, 0, param.rise*2+param.top-1);
	checkCudaErrors(cudaMalloc(&d_dotTrapFilter, sizeof(double)*(param.rise * 2 + param.top)));
	checkCudaErrors(cudaMemcpy(d_dotTrapFilter, dotTrapFilter, sizeof(double)*(param.rise * 2 + param.top), cudaMemcpyHostToDevice));
}

//------------------------------
//        fftFilters
//------------------------------
void FILTERS::fftFilters(PARAMS &param) {
	//no matter what the trap filter is done in single precision so handle that
	//need this plan always
	checkCudaErrors(cufftPlan1d(&planSingleFilter, param.padWavelength, CUFFT_D2Z, 1));
	checkCudaErrors(cudaDeviceSynchronize());
	//do the fft plans
	checkCudaErrors(cudaDeviceSynchronize());
	checkCudaErrors(cufftPlan1d(&planBasisFunctions, param.padWavelength, CUFFT_D2Z, numFitFunctions));
	checkCudaErrors(cudaDeviceSynchronize());
	//allocate space for the results of the ffts
	checkCudaErrors(cudaMalloc(&d_fftBoxFilter, param.paddedFFTLength * sizeof(double2)));
	checkCudaErrors(cudaDeviceSynchronize());
	checkCudaErrors(cudaMalloc(&d_fftInverseFitFunctions, param.paddedFFTLength * numFitFunctions * sizeof(double2)));
	checkCudaErrors(cudaDeviceSynchronize());
	//execute the plans	
	if(weighting == 0 || weighting == 1)
		checkCudaErrors(cufftExecD2Z(planSingleFilter, d_boxFilter, d_fftBoxFilter));
	checkCudaErrors(cudaDeviceSynchronize());
	checkCudaErrors(cufftExecD2Z(planBasisFunctions, d_inverseFitFunctions, d_fftInverseFitFunctions));
	checkCudaErrors(cudaDeviceSynchronize());
}


__global__ void invertSingularValues(double *input, double *output, int numRow, int numCol){
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	int row = i % numRow;
	int col = i / numRow;
	if(row == col)
		output[row + col * numRow] = 1.0 / input[row];
	else
		output[row + col * numRow] = 0.0;
}


void FILTERS::calculatePseudoInverse(double *inputFunc, double **outputFunc, int numRows, int numCols){
	//create the various handles that will need to be used
	cusolverDnHandle_t cusolverH = NULL;
	cusolverStatus_t status = CUSOLVER_STATUS_SUCCESS;
	status = cusolverDnCreate(&cusolverH);	

	const int m = numRows;
	const int n = numCols;	
	if(n > m){
		std::cerr<<"numRows must be >= numCols in calculatePseudoInverse"<<std::endl;
		exit(-1);
	}
	//create the various storage containers that will be needed during this
	int *devInfo;
	double *d_S, *d_U, *d_VT;
	double *workSpace;
	double *rWorkSpace = NULL;
	cudaMalloc(&d_S, sizeof(double) * n);
	cudaMalloc(&d_U, sizeof(double) * m * m);
	cudaMalloc(&d_VT, sizeof(double) * n * n);
	cudaMalloc(&devInfo, sizeof(int));
	//create the buffer
	//first check the size
	int lWorkSize = 0;
	status = cusolverDnDgesvd_bufferSize(cusolverH, m, n, &lWorkSize);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaMalloc(&workSpace, sizeof(double) * lWorkSize));
	//now do the calculation itself
	checkCudaErrors(cudaDeviceSynchronize());
	status = cusolverDnDgesvd(
		cusolverH,
		'A',
		'A',
		m,
		n,
		inputFunc,
		m,
		d_S,
		d_U,
		m,
		d_VT,
		n,
		workSpace,
		lWorkSize,
		rWorkSpace,
		devInfo);
	checkCudaErrors(cudaDeviceSynchronize());
	if(status != 0){
		std::cerr<<"errors in the cuSOLVER operations: "<<status<<std::endl;
		exit(0);
	}
	//at this point the inversion has been done
	//now we need to calculate the pseudoinverse
	//first inverse the singular values
	double *d_sMatrix;
	checkCudaErrors(cudaMalloc(&d_sMatrix, sizeof(double) * n * m));
	invertSingularValues<<<n * m, 1>>>(d_S, d_sMatrix, n, m);
	checkCudaErrors(cudaDeviceSynchronize());
	cublasStatus_t cublasStatus;
	cublasHandle_t cublasHandle;
	cublasStatus = cublasCreate(&cublasHandle);
	//do the multiplication of the S inverted matrix and U^T matrix
	double alpha = 1.0;
	double beta = 0.0;
	double *d_W;//temporary storage for matrix multiplication results
	checkCudaErrors(cudaMalloc(&d_W, sizeof(double) * n * m));
	cublasStatus = cublasDgemm(cublasHandle,
		CUBLAS_OP_N, CUBLAS_OP_T,
		n, m, m,
		&alpha, 
		d_sMatrix, n,
		d_U, m,
		&beta,
		d_W, n);
	checkCudaErrors(cudaDeviceSynchronize());
	//now do the matrix multiplication of V and d_W
	checkCudaErrors(cudaMalloc(outputFunc, sizeof(double) * n * m));
	cublasStatus = cublasDgemm(cublasHandle,
		CUBLAS_OP_T, CUBLAS_OP_N,
		n, m, n,
		&alpha,
		d_VT, n,
		d_W, n,
		&beta,
		*outputFunc, n);
	checkCudaErrors(cudaDeviceSynchronize());

	if(cublasStatus != 0){
		std::cerr<<"error in cublas functions: "<<cublasStatus<<std::endl;
		exit(1);
	}
	//now free out the temporary variables we don't need anymore
	cudaFree(devInfo);
	cudaFree(d_S);
	cudaFree(d_U);
	cudaFree(d_VT);
	cudaFree(d_W);

}


//this function creates the matrix that will be used to fit the chi squared minima
//to a quadratic function
void FILTERS::defChiSquareFit(PARAMS &param){
	//define the functions themselves
	double *quadFitFuncs; 
	//go through and make the fit functions on CPU since that's easy enough to do
	quadFitFuncs = (double*)malloc(sizeof(double) * quadFitLen * 3);
	for(int i = 0; i < quadFitLen; i++){
		quadFitFuncs[i] = 1.0;
		quadFitFuncs[i+quadFitLen] = i;
		quadFitFuncs[i+2*quadFitLen] = i * i;
	}
	double *d_quadFitFuncs;	
	//move to the GPU
	checkCudaErrors(cudaMalloc(&d_quadFitFuncs, sizeof(double) * quadFitLen * 3));		
	checkCudaErrors(cudaMemcpy(d_quadFitFuncs, quadFitFuncs, sizeof(double) * quadFitLen * 3, cudaMemcpyHostToDevice));
	//now do the pseudoinverse with that dedicated function
	calculatePseudoInverse(d_quadFitFuncs, &d_quadFitMatrix, quadFitLen, 3);	
	//at this point we have the pseudoinverse calculated for the quadratic fit matrix
	checkCudaErrors(cudaDeviceSynchronize());
	//check the desired precision
	free(quadFitFuncs);
	checkCudaErrors(cudaFree(d_quadFitFuncs));
}

//-------------------------------
//        Destructor
//-------------------------------
FILTERS::~FILTERS(){
	//in order of occurence in this file
	free(fitFunctions);
	checkCudaErrors(cudaFree(d_fitFunctions));
	free(inverseFitFunctions);
	checkCudaErrors(cudaFree(d_inverseFitFunctions));
	free(fitMatrixSquared);
	checkCudaErrors(cudaFree(d_fitMatrixSquared));
	if(weighting == 0 || weighting == 1){
		free(h_boxFilter);
		checkCudaErrors(cudaFree(d_boxFilter));
	}
	else{
		free(h_weightMatrix);
		checkCudaErrors(cudaFree(d_weightMatrix));
	}
	//now that stuff has been freed out
	//do the trapezoidal filter now
	//now free out the fft plans and whatnot
	checkCudaErrors(cufftDestroy(planBasisFunctions));
	checkCudaErrors(cufftDestroy(planSingleFilter));
	checkCudaErrors(cudaFree(d_fftBoxFilter));
	checkCudaErrors(cudaFree(d_fftInverseFitFunctions));
	//free out the quadratic fit functioni
	if(quadFitLen > 0){
		checkCudaErrors(cudaFree(d_quadFitMatrix));
	}
	free(enerScales);
	checkCudaErrors(cudaFree(d_enerScales));
	if(numIdealPulses > 1){
		if(psdWeight > 0){
			free(psdYTYWeight);
			checkCudaErrors(cudaFree(d_psdYTYWeight));
		}
		for(int i = 0; i < numIdealPulses; i++){
			free(psdInverses[i]);
			cudaFree(d_psdInverses[i]);
			free(psdATA[i]);
			checkCudaErrors(cudaFree(d_psdATA[i]));
		}
		free(psdInverses);
		free(d_psdInverses);
		free(psdATA);
		free(d_psdATA);
	}
}
}  // Namespace gpuAnalysis
