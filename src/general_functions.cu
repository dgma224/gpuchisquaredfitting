/*
general_functions.cu
This file contains the definitions of many of the file I/O
*/
#ifndef __GENERAL_FUNCTIONS_CU_INCLUDED__
#define __GENERAL_FUNCTIONS_CU_INCLUDED__

#include "gpu_functions.h"
#include "waves.h"
#include "params.h"
#include "debug.h"
#include <stdio.h>


namespace gpuAnalysis{

struct WAVEFORM{
	bool result;
	int eventID;
	int board;
	int channel;
	unsigned long timestamp;
	unsigned long req; //for ca45 compatibility
	int length;
	int complength;
	short firstval;
};

//do the waveform reading
WAVEFORM readHead(FILE *fin, int fileFormat) {
  WAVEFORM head;
  fread(&head.result, sizeof(bool), 1, fin);
  fread(&head.eventID, sizeof(int), 1, fin);
	fread(&head.board, sizeof(int), 1, fin);
	fread(&head.channel, sizeof(int), 1, fin);
  fread(&head.timestamp, sizeof(unsigned long), 1, fin);
	if(fileFormat == -2 || fileFormat == 0)//ca45 format or nab format
		fread(&head.req, sizeof(unsigned long), 1, fin);
  fread(&head.length, sizeof(int), 1, fin);
  return head;
}

WAVEFORM readHeadComp(FILE *fin){
  WAVEFORM head;
  fread(&head.result, sizeof(bool), 1, fin);
  fread(&head.eventID, sizeof(int), 1, fin);
  fread(&head.board, sizeof(int), 1, fin);
  fread(&head.channel, sizeof(int), 1, fin);
  fread(&head.timestamp, sizeof(unsigned long), 1, fin);
  fread(&head.length, sizeof(int), 1, fin);
  fread(&head.complength, sizeof(int), 1, fin);
  fread(&head.firstval, sizeof(short), 1, fin);
  return head;
}

void readWave(FILE *fin, short *wave, int length) {
  fread(wave, sizeof(short), length, fin);
}

void readWaveComp(FILE *fin, unsigned int *wave, unsigned short length) {
  fread(wave, sizeof(int), length, fin);
}

void writeWave(FILE *fout, short *wave, int length) {
  fwrite(wave, sizeof(short), length, fout);
}

void writeHead(FILE *fout, WAVEFORM head, int fileFormat){
	fwrite(&head.result, sizeof(bool), 1, fout);
  fwrite(&head.eventID, sizeof(int), 1, fout);
  fwrite(&head.board, sizeof(int), 1, fout);
  fwrite(&head.channel, sizeof(int), 1, fout);
  fwrite(&head.timestamp, sizeof(unsigned long), 1, fout);
	if(fileFormat == -2 || fileFormat == 0)//the case of Ca45 or current Nab format
		fwrite(&head.req, sizeof(unsigned long), 1, fout);
  fwrite(&head.length, sizeof(int), 1, fout);
}

//determine how many blocks and threads should be used
int calcNumBlocks(int len, int threads) {
  // If a remainder, add extra block
  return (len % threads) ? len / threads + 1 : len / threads;
}


//File Reading
void readBatch(FILE *fin, WAVES& waves, WAVEFORM *heads, int toread,
    PARAMS param) {
	if(param.isCompressed == false){
		for (int i = 0; i < toread; i++) {
			heads[i]=readHead(fin, param.fileFormat);
			readWave(fin, &waves.waveforms[i*param.padWavelength], param.wavelength);
		}
	}
	else{
		for (int i = 0; i < toread; i++){
			heads[i]=readHeadComp(fin);
			readWaveComp(fin, &waves.compwaves[i*param.maxCompLen], heads[i].complength);
		}
	}	
}



//File Writing
void writeBatch(FILE *fout, short *waves, WAVEFORM* heads, int towrite,
    PARAMS param){
  for(int i = 0; i < towrite; i++) {
    writeHead(fout, heads[i], param.fileFormat);
    writeWave(fout, &waves[i*param.wavelength], param.wavelength);
  }
}

void writeResult(FILE *fout, Complex *results, WAVEFORM *heads, int toWrite, PARAMS param){
  // For every wave
  for (int i = 0; i < toWrite; i++) {
    writeHead(fout, heads[i], param.fileFormat);  // Write header
    fwrite(&results[i].x, sizeof(float), 1, fout);  // Write energy
    fwrite(&results[i].y, sizeof(int), 1, fout);  // Write t0
  }
}


//General Setup functions
void processFileHeaders(FILE *&fin, FILE*&fout, PARAMS &param){
	//this function handles reading and writing the headers for the overall file
	//first output the file format that was used so the output code knows what it's dealing with
	fwrite(&param.fileFormat, sizeof(int), 1, fout);
	if(param.fileFormat == -2){//in the case of Ca45 format
		//header is just an 8 byte double
		double header = 0.0;
		fread(&header, sizeof(double), 1, fin);
		fwrite(&header, sizeof(double), 1, fout);
	}
	else if(param.fileFormat == -1){//in the case of old nab format
		// in this case there is no file header
		//the easiest case to handle
	}
	else if(param.fileFormat == 0){//in the case of the new nab format
		//in this case there is a file header and it needs to be parsed
		//read in what type of nab file it is
		int nabType = 0;
		fread(&nabType, sizeof(int), 1, fin);
		if(nabType == 0){//this is the only format currently defined
			//need to read in how long the following header part is
			int headerLength = 0;
			fread(&headerLength, sizeof(int), 1, fin);
			//write that to the output as well
			fwrite(&headerLength, sizeof(int), 1, fout);
			//now read in that entire header and write it out
			char *temp = (char*)malloc(sizeof(char) * headerLength);
			fread(&temp, sizeof(char), headerLength, fin);
			//and now write it out
			fwrite(&temp, sizeof(char), headerLength, fin);
		}
		else{
			std::cerr<<"unrecognized Nab file format type: ID "<<nabType<<std::endl;
			std::cerr<<"code exiting"<<std::endl;
			exit(1);
		}
	}
}

void fileSetUp(FILE *&fin, FILE*&fout, int fileNum, PARAMS &param, FILTERS &filters){
	fin = fopen(param.datafiles.at(fileNum).c_str(), "rb");
	if(!fin){
		perror("Halp");
		std::cerr << param.datafiles.at(fileNum) <<" couldn't be opened: Code Exiting\n";
		exit(1);
	}
	//once that file is open, create the results file
	std::string outname;
	size_t perloc = param.datafiles.at(fileNum).find_last_of(".");
	outname = param.datafiles.at(fileNum).substr(0,perloc);
	outname = outname + ".fitres";
	fout = fopen(outname.c_str(), "wb");
	processFileHeaders(fin, fout, param);
	//now with that done write the header information specific to the output file format
	//first we need to say what the file format is, Ca45 or Nab
	//first number of pulse shape filters
	fwrite(&filters.numIdealPulses, sizeof(int), 1, fout);
	//now how many filters there were total
	fwrite(&filters.numFitFunctions, sizeof(int), 1, fout);
	//at this point the file is set up, except for the Ca45	 
}

//this function transfers the read in waveforms to GPU

void sendToGPU(WAVES& wave, PARAMS& param){
	if(param.isCompressed){
		cudaMemcpyAsync(wave.d_firsts, wave.firsts, param.shortSize,
			cudaMemcpyHostToDevice, wave.stream);
		cudaMemcpyAsync(wave.d_complengths, wave.complengths, param.intSize,
			cudaMemcpyHostToDevice, wave.stream);
		cudaMemcpyAsync(wave.d_compwaves, wave.compwaves, param.intSizeComp,
			cudaMemcpyHostToDevice, wave.stream);
	}
	else{
		cudaMemcpyAsync(wave.d_waveforms, wave.waveforms, param.shortSizeBatch,
			cudaMemcpyHostToDevice, wave.stream);
	}
}

//This function does the initial setup and then the Fourier Transform of the waveforms
//For compressed waveforms, it also handles the de-compression
void wavesProcessing(WAVES &waves, PARAMS &param) {
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	/*
  Takes a WAVES struct and preforms andFunction and baseline shifting, 
  converts to floats,and applies an FFT
  */
	if(param.isCompressed){
		//need to do the de-compression
		gpuDecompression<<<param.baseBlocks, param.baseThreads, 0, waves.stream>>>
			(waves.d_waveforms, waves.d_compwaves, waves.d_complengths, waves.d_firsts, param.wavelength, param.padWavelength, param.maxCompLen, param.batchSize);
	}
	else{
		andFunctionPad<<<param.andBlocks, param.andThreads, 0, waves.stream>>>
    (waves.d_waveforms, param.wavelength, param.padWavelength, param.batchSize * param.padWavelength);
	}
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	//convert the data to the either floats or doubles
	typeConversion<<<param.andBlocks, param.andThreads, 0, waves.stream>>>(waves.d_waveforms, waves.d_doubleWaveforms, param.padWavelength * param.batchSize);
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	//do the FFT
	cufftExecD2Z(waves.planWavesForward, waves.d_doubleWaveforms, waves.d_complexWaveforms);
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	//calcualte the yTy values in the simple case
	squareWaveforms<<<param.andBlocks, param.andThreads, 0, waves.stream>>>(waves.d_doubleWaveforms, waves.d_waveformSquared, param.batchSize * param.padWavelength);
	cufftExecD2Z(waves.planWavesForward, waves.d_waveformSquared, waves.d_fftWaveformSquared);
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
}

//this function applies all of the FFT Filters to the waveforms
//this also finds the relevant fit locations for use later on
void applyFilters(WAVES &waves, FILTERS &filters, PARAMS &param){
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	//apply the fit functions
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	applyFitFunctionFilters<<<param.mulBlocks, param.mulThreads, 0, waves.stream>>>(waves.d_complexWaveformsCopy, waves.d_complexWaveforms, filters.d_fftInverseFitFunctions, param.paddedFFTLength, filters.numFitFunctions, param.paddedFFTLength * param.batchSize);
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	//now do the inverse FFTs on these results
	cufftExecZ2D(waves.planFitParamsInverse, waves.d_complexWaveformsCopy, waves.d_fitParameters);
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	calculateSumIdealFitParameters<<<param.andBlocks, param.andThreads, 0, waves.stream>>>(waves.d_sumFitParams, waves.d_fitParameters, filters.numIdealPulses, filters.numFitFunctions, param.padWavelength, param.batchSize, param.padWavelength * param.batchSize);
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	//now find the maximum values of the sum of the fit parameters
	findMaxLocIdealFitParameter<<<param.baseBlocks, param.baseThreads, 0, waves.stream>>>(waves.d_maxFitLocs, waves.d_sumFitParams, filters.fitFunctionLength, param.padWavelength, int(filters.fitFunctionLength/2), param.batchSize, param.searchRange, param.batchSize);
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
}

void calculateYTWY(WAVES &waves, FILTERS &filters, PARAMS &param){
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	
	if(filters.weighting == 0 || filters.weighting == 1){
		//first calculate all of the YTY values for these weights
		//then go through and mna
		//now apply the boxcar filter
		multiplication<<<param.mulBlocks, param.mulThreads, 0, waves.stream>>>(waves.d_fftWaveformSquared, filters.d_fftBoxFilter, param.paddedFFTLength, param.batchSize * param.paddedFFTLength);
		//then undo the FFT
		cufftExecZ2D(waves.planSquaredInverse, waves.d_fftWaveformSquared, waves.d_waveformSquared);
		//now grab the relevant parts of that output
		grabRelevantElementsYTY<<<param.searchBlocks, param.searchThreads, 0, waves.stream>>>(waves.d_relevantYTY, waves.d_waveformSquared, waves.d_maxFitLocs, param.padWavelength, param.searchRange, param.searchLen, param.batchSize * param.searchLen);
	}
	else{
		//now the 2d version of this function, how to go about doing this?
		//need to first grab the relevant sections of the waveform
		int threads = 1024;
		int blocks = calcNumBlocks(filters.fitFunctionLength * param.batchSize, threads); 
		checkCudaErrors(cudaStreamSynchronize(waves.stream));
		grabRelevantWaveformPieces<<<blocks, threads, 0, waves.stream>>>(waves.d_relevantWaveformPieces, waves.d_doubleWaveforms, waves.d_maxFitLocs, param.padWavelength, param.searchRange, param.searchLen, filters.fitFunctionLength, filters.fitFunctionLength * param.batchSize);
		checkCudaErrors(cudaStreamSynchronize(waves.stream));
		//with these picked off it's time to do the multiplication and whatnot
		double alpha = 1.0;
		double beta = 0.0;
		checkCudaErrors(cudaStreamSynchronize(waves.stream));
		waves.cublasStatus = cublasDgemmStridedBatched(
			waves.cublasHandle,
			CUBLAS_OP_N,
			CUBLAS_OP_N,
			filters.fitFunctionLength,
			1,
			filters.fitFunctionLength,
			&alpha,
			filters.d_weightMatrix,
			filters.fitFunctionLength,
			0,
			waves.d_relevantWaveformPieces,
			filters.fitFunctionLength,
			filters.fitFunctionLength,
			&beta,
			waves.d_temporaryYTYValues,
			filters.fitFunctionLength, 
			filters.fitFunctionLength,
			param.batchSize * param.searchLen);
		//now that the Wy multiplication is done, do the yT W part
		checkCudaErrors(cudaStreamSynchronize(waves.stream));
		waves.cublasStatus = cublasDgemmStridedBatched(
			waves.cublasHandle,
			CUBLAS_OP_T,
			CUBLAS_OP_N,
			1, 
			1,
			filters.fitFunctionLength,
			&alpha,
			waves.d_relevantWaveformPieces,
			filters.fitFunctionLength,
			filters.fitFunctionLength,
			waves.d_temporaryYTYValues,
			filters.fitFunctionLength,
			filters.fitFunctionLength,
			&beta, 
			waves.d_relevantYTY,
			1,
			1,
			param.batchSize * param.searchLen);
	}
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
}

//this function goes and calculates the other part of the chi squared parameters
//it does this with cublas and matrix multiplications of course
void calculatexTATAx(WAVES &waves, FILTERS &filters, PARAMS &param){	
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	//grab the relevant fit parameters to be used in the fitting process
	//determine the number of blocks and threads to be used really quickly
	int threads = 1024;
	int totRange = param.batchSize * filters.numFitFunctions * param.searchLen;
	int blocks = calcNumBlocks(totRange, threads);
	grabRelevantElementsX<<<blocks, threads, 0, waves.stream>>>(waves.d_relevantFitParams, waves.d_fitParameters, waves.d_maxFitLocs, param.padWavelength, param.searchRange, param.searchLen, filters.numFitFunctions, totRange);
	//now that these have been grabbed, we need to do the matrix multiplication operation using cuBLAS
	double oneScale = 1.0;
	double zeroScale = 0.0;
	waves.cublasStatus = cublasDgemmStridedBatched(
		waves.cublasHandle,
		CUBLAS_OP_N,
		CUBLAS_OP_N,
		filters.numFitFunctions,
		1,
		filters.numFitFunctions,
		&oneScale,
		filters.d_fitMatrixSquared,
		filters.numFitFunctions,
		0,
		waves.d_relevantFitParams,
		filters.numFitFunctions,
		filters.numFitFunctions,
		&zeroScale,
		waves.d_tempxATAx,	
		filters.numFitFunctions, 
		filters.numFitFunctions,
		param.batchSize * param.searchLen);
	//this does the second multiplication xATWAx
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	waves.cublasStatus = cublasDgemmStridedBatched(
		waves.cublasHandle,
		CUBLAS_OP_T,
		CUBLAS_OP_N,
		1,
		1,
		filters.numFitFunctions,
		&oneScale,
		waves.d_relevantFitParams,
		filters.numFitFunctions,
		filters.numFitFunctions,
		waves.d_tempxATAx,
		filters.numFitFunctions,
		filters.numFitFunctions,
		&zeroScale,
		waves.d_xATAx,
		1,
		1,
		param.batchSize * param.searchLen);
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
}

//this function goes through and calculates all of the chi squared values that we are searching over
void findChiSquared(WAVES &waves, FILTERS &filters, PARAMS &param){
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	calculateChiSquared<<<param.andBlocks, param.andThreads, 0, waves.stream>>>(waves.d_chiSquared, waves.d_relevantYTY, waves.d_xATAx, param.batchSize * param.searchLen);
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
}

//This function finds the minimum chi squared in each array
//it does this by finding the maximum value of the sum of 
//the ideal fit parameters and searching near that value
void findMinChiSquared(WAVES &waves, FILTERS &filters, PARAMS &param){
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	//this function needs to output the minimum chi squared, it's location, and the fit parameters
	//output to waves.d_minChiSquaredFloat, waves.d_minChiSquaredLoc, waves.d_fitResultsFloat/Double
	//that correspond with that
	if(param.searchRange == 0){
		//in this case, we don't have to do any searching, the initial values are assumed to be correct
		noSearchGrabValues<<<param.baseBlocks, param.baseThreads>>>(waves.d_minChiSquared, waves.d_minChiSquaredLoc, waves.d_fitResults, waves.d_chiSquared, waves.d_maxFitLocs, waves.d_fitParameters, filters.numFitFunctions, filters.fitFunctionLength, filters.pretrigger, param.padWavelength, param.batchSize);
	}
	else if(param.searchRange != 0 && param.quadFitLen == 0){
		//in this case we do some searching around where the minimum is to try to find it
		//but we don't go through and fit the minima
		localChiSquaredMinSearch<<<param.baseBlocks, param.baseThreads, 0, waves.stream>>>(waves.d_minChiSquared, waves.d_minChiSquaredLoc, waves.d_fitResults, waves.d_chiSquared, waves.d_maxFitLocs, waves.d_fitParameters, param.searchRange, param.searchLen, filters.fitFunctionLength, filters.pretrigger, param.padWavelength, filters.numFitFunctions, param.batchSize); 
	}
	else{
		//this is the case where we need to search for a minima
		//then fit the chi squared values around that minima
		chiMinSearchAndGrab<<<param.baseBlocks, param.baseThreads, 0, waves.stream>>>(waves.d_fitChiSquared, waves.d_chiSquared, waves.d_minChiSquaredLoc, param.searchRange, param.searchLen, param.quadFitLen, param.batchSize);
		double alpha=1.0, beta=0.0;
		waves.cublasStatus = cublasDgemmStridedBatched(waves.cublasHandle,
			CUBLAS_OP_N, CUBLAS_OP_N,
			3, 1, param.quadFitLen,	
			&alpha,
			filters.d_quadFitMatrix, 3, 0,
			waves.d_fitChiSquared, param.quadFitLen, param.quadFitLen,
			&beta, 
			waves.d_chiSquaredFitParams, 3, 3,
			param.batchSize);
		calculateChiFitMin<<<param.baseBlocks, param.baseThreads, 0, waves.stream>>>(waves.d_minChiSquared, waves.d_minChiSquaredLoc, waves.d_chiSquaredFitParams, waves.d_maxFitLocs, waves.d_fitResults, waves.d_relevantFitParams, param.searchRange, param.searchLen, param.quadFitLen, filters.numFitFunctions, filters.fitFunctionLength, filters.pretrigger, param.batchSize);	
	}
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
}

void findEnergy(WAVES &waves, FILTERS & filters, PARAMS &param){
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	//now just go through and scale the fit parameters by the scale factors
	double alpha = 1.0, beta = 0.0;
	waves.cublasStatus = cublasDgemmStridedBatched(waves.cublasHandle,
		CUBLAS_OP_N, CUBLAS_OP_T,
		1, 1, filters.numFitFunctions,
		&alpha,
		waves.d_fitResults, 1, filters.numFitFunctions,
		filters.d_enerScales, 1, 0,
		&beta,
		waves.d_trapResultsDouble, 1, 1,
		param.batchSize);
}

//this function uses the projection operator method to do pulse shape discrimination
void findPulseShapes(WAVES &waves, FILTERS& filters, PARAMS &param){
	//first grab the relevant waveform pieces to run this over	
	if(filters.numIdealPulses > 1){
		int numFilters = filters.numFitFunctions - filters.numIdealPulses + 1;
		int threads = 1024;
		int blocks = calcNumBlocks(filters.fitFunctionLength * param.batchSize, threads); 
		checkCudaErrors(cudaStreamSynchronize(waves.stream));
		grabRelevantWaveformPiecesForFit<<<blocks, threads, 0, waves.stream>>>(waves.d_relevantFitPieces, waves.d_doubleWaveforms, waves.d_minChiSquaredLoc, param.padWavelength, filters.fitFunctionLength, filters.pretrigger, filters.fitFunctionLength * param.batchSize);
		checkCudaErrors(cudaStreamSynchronize(waves.stream));
		//with the waveform chunks grabbed, need to go through and get the results
		double alpha = 1.0, beta = 0.0;
		for(int i = 0; i < filters.numIdealPulses; i++){
			//first step in the proces is the yty calculation		
			checkCudaErrors(cudaStreamSynchronize(waves.stream));
			if(filters.psdWeight == 0){
				waves.cublasStatus = cublasDgemmStridedBatched(waves.cublasHandle,
					CUBLAS_OP_N, CUBLAS_OP_T,
					1, 1, filters.fitFunctionLength,
					&alpha,
					waves.d_relevantFitPieces, 1, filters.fitFunctionLength,
					waves.d_relevantFitPieces, 1, filters.fitFunctionLength,
					&beta,
					waves.d_psdYTY, 1, 1,
					param.batchSize);
				checkCudaErrors(cudaStreamSynchronize(waves.stream));
			}
			else if(filters.psdWeight == 1){
				//first do the pointwise multiplication
				multiplication<<<blocks, threads, 0, waves.stream>>>(waves.d_relevantFitPieces, filters.d_psdYTYWeight, waves.d_psdTempYTY, filters.fitFunctionLength, filters.fitFunctionLength * param.batchSize);
				//with that done to the dot product
				waves.cublasStatus = cublasDgemmStridedBatched(waves.cublasHandle,
					CUBLAS_OP_N, CUBLAS_OP_T,
					1, 1, filters.fitFunctionLength,
					&alpha,
					waves.d_psdTempYTY, 1, filters.fitFunctionLength,
					waves.d_relevantFitPieces, 1, filters.fitFunctionLength,
					&beta,
					waves.d_psdYTY, 1, 1,
					param.batchSize);
			}
			else{
				waves.cublasStatus = cublasDgemmStridedBatched(waves.cublasHandle,
					CUBLAS_OP_N, CUBLAS_OP_T,
					filters.fitFunctionLength, 1, filters.fitFunctionLength,
					&alpha,
					filters.d_psdYTYWeight, filters.fitFunctionLength, 0,
					waves.d_relevantFitPieces, 1, filters.fitFunctionLength,
					&beta, 
					waves.d_psdTempYTY, filters.fitFunctionLength, filters.fitFunctionLength,
					param.batchSize);
				waves.cublasStatus = cublasDgemmStridedBatched(waves.cublasHandle,
					CUBLAS_OP_N, CUBLAS_OP_T,
					1, 1, filters.fitFunctionLength,
					&alpha,
					waves.d_psdTempYTY, 1, filters.fitFunctionLength,
					waves.d_relevantFitPieces, 1, filters.fitFunctionLength,
					&beta,
					waves.d_psdYTY, 1, 1,
					param.batchSize);
			}
			checkCudaErrors(cudaStreamSynchronize(waves.stream));
			//with the yty values determined, now go about finding the fit parameters
			waves.cublasStatus = cublasDgemmStridedBatched(waves.cublasHandle,
				CUBLAS_OP_N, CUBLAS_OP_T,
				numFilters, 1, filters.fitFunctionLength,
				&alpha,
				filters.d_psdInverses[i], numFilters, 0,
				waves.d_relevantFitPieces, 1, filters.fitFunctionLength,
				&beta,
				waves.d_psdParams, numFilters, numFilters,
				param.batchSize);	
			checkCudaErrors(cudaStreamSynchronize(waves.stream));
			//now do the xATWAx calculation
			
			waves.cublasStatus = cublasDgemmStridedBatched(waves.cublasHandle,
				CUBLAS_OP_N, CUBLAS_OP_T,
				numFilters, 1, numFilters,
				&alpha,
				filters.d_psdATA[i], numFilters, 0,
				waves.d_psdParams, 1, numFilters,
				&beta,
				waves.d_psdTempATA, numFilters, numFilters,
				param.batchSize);
			checkCudaErrors(cudaStreamSynchronize(waves.stream));
			waves.cublasStatus = cublasDgemmStridedBatched(waves.cublasHandle,
				CUBLAS_OP_N, CUBLAS_OP_T,
				1, 1, numFilters,
				&alpha,
				waves.d_psdParams, 1, numFilters,
				waves.d_psdTempATA, numFilters, numFilters,
				&beta,
				waves.d_psdATA, 1, 1,
				param.batchSize);	
			checkCudaErrors(cudaStreamSynchronize(waves.stream));
			calculateChiSquared<<<param.baseBlocks, param.baseThreads , 0, waves.stream>>>(&waves.d_PSDChis[param.batchSize*i], waves.d_psdYTY, waves.d_psdATA, param.batchSize);
		}
		threads = 32;
		blocks = calcNumBlocks(param.batchSize, threads);
		gpuPSD<<<blocks, threads, 0, waves.stream>>>(waves.d_psdPredictions, waves.d_percents, waves.d_PSDChis, filters.numIdealPulses, param.batchSize);
		checkCudaErrors(cudaStreamSynchronize(waves.stream));
		//debug::d_arrayWrite(waves.d_PSDChis, param.batchSize*filters.numIdealPulses, "chis.txt");
		//debug::d_arrayWrite(waves.d_psdPredictions, param.batchSize, "predictions.txt");
		//exit(0);
	}
}


void getResultsFromGPU(WAVES &waves, FILTERS& filters, PARAMS &param){
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	//simply send the results back to the CPU
	cudaMemcpyAsync(waves.fitResults, waves.d_fitResults, param.doubleSize*filters.numFitFunctions, cudaMemcpyDeviceToHost, waves.stream);
	//send the energies
	cudaMemcpyAsync(waves.trapResults, waves.d_trapResults, param.floatSize, cudaMemcpyDeviceToHost, waves.stream);
	//now send the chi squared values
	cudaMemcpyAsync(waves.minChiSquared, waves.d_minChiSquared, param.doubleSize, cudaMemcpyDeviceToHost, waves.stream);
	//send back the minChiSquared locations
	cudaMemcpyAsync(waves.minChiSquaredLoc, waves.d_minChiSquaredLoc, param.floatSize, cudaMemcpyDeviceToHost, waves.stream);
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	if(filters.numIdealPulses > 1){
		cudaMemcpyAsync(waves.psdPredictions, waves.d_psdPredictions, param.intSize, cudaMemcpyDeviceToHost, waves.stream);
		cudaMemcpyAsync(waves.percents, waves.d_percents, param.floatSize, cudaMemcpyDeviceToHost, waves.stream);
	}
}

//this function converts the header to the .csv string format
std::string getHeaderString(WAVEFORM header, int fileFormat){
	std::string output;
	if(fileFormat == 1){
		output = 
			std::to_string(header.result) + ", " +
			std::to_string(header.eventID) + ", " +
			std::to_string(header.board) + ", " +
			std::to_string(header.channel) + ", " +
			std::to_string(header.timestamp) + ", " +
			std::to_string(header.req) + ", " + 
			std::to_string(header.length) + ", ";
	}
	else{
		output = 
			std::to_string(header.result) + ", " +
			std::to_string(header.eventID) + ", " +
			std::to_string(header.board) + ", " +
			std::to_string(header.channel) + ", " +
			std::to_string(header.timestamp) + ", " +
			std::to_string(header.length) + ", ";
	}
	return output;
}

void printHeaderCSV(WAVEFORM header, int dataFormat){
	std::cout<<header.result<<", "<<header.eventID<<", "<<header.board<<", "<<header.channel<<", "<<header.timestamp;
	if(dataFormat == 1){ //ca45 version
		std::cout<<", "<<header.req;
	}
	std::cout<<", "<<header.length;
}

void printCSVResults(WAVEFORM header, float trapResult, float minLoc, float minVal, float *params, int numParams, int dataFormat){
	printHeaderCSV(header, dataFormat);
	std::cout<<", "<<trapResult<<", "<<minLoc<<", "<<minVal;
	for(int i = 0; i < numParams; i++){
		std::cout<<", "<<params[i];
	}
	std::cout<<std::endl;
}


void writeResults(FILE *fout, WAVEFORM* headers, WAVES &waves, FILTERS &filters, int numToWrite, PARAMS &param){
	//the results are already on the CPU at this point
	//we just need to write them in the csv format
	checkCudaErrors(cudaStreamSynchronize(waves.stream));
	for(int i = 0; i < numToWrite;	i++){
		//first write the header
    writeHead(fout, headers[i], param.fileFormat);
		//now write the energy
		fwrite(&waves.trapResults[i], sizeof(float), 1, fout);
		//write the timestamp
		float temp = float(waves.minChiSquaredLoc[i]);
		fwrite(&temp, sizeof(float), 1, fout);
		//write the chi squared value
		temp = float(waves.minChiSquared[i]);
		fwrite(&temp, sizeof(float), 1, fout);
		//now iterate over the fit functions
		for(int fitFunc = 0; fitFunc < filters.numFitFunctions; fitFunc++){
			temp = float(waves.fitResults[i*filters.numFitFunctions + fitFunc]);
			fwrite(&temp, sizeof(float), 1, fout);
		}
		if(filters.numIdealPulses > 1){
			fwrite(&waves.psdPredictions[i], sizeof(int), 1, fout);
			fwrite(&waves.percents[i], sizeof(float), 1, fout);
		}
	}
}


}//namespace gpuAnalysis

#endif
