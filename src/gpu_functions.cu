// gpu_functions.cu
#include "gpu_functions.h"
#include "waves.h"
#include <stdio.h>

typedef long long int superint; //64 bit container for reading in compressed data
namespace gpuAnalysis {

//-------------------------------------
//-------------------------------------
//					GPU Code
//-------------------------------------
//-------------------------------------

//squaring a long array of data on the GPU
__global__ void squareWaveforms(double * inputWave, double *outputWave, int totalNumberDataPoints){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id < totalNumberDataPoints){
		outputWave[id] = pow(inputWave[id], 2.0);
	}
}

//-------------------------------------
//  applyFitFunctions (complex multiplication with many fit functions)
//-------------------------------------
__global__ void applyFitFunctionFilters(ComplexD * output, ComplexD *inputWaveforms, ComplexD *filters, int fftLength, int numFitFunctions, int totalNumberDataPoints){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id < totalNumberDataPoints){//do work
		//first load in the data point from the input waveforms
		ComplexD inputWave = inputWaveforms[id];
		//now begin the loop of calculations
		ComplexD temp;//setup storage for the calculations
		int waveLoc = id % fftLength;//determines where in the waveform we are	
		int waveNum = int(id / fftLength);//determines what waveform we are in
		for(int filt = 0; filt < numFitFunctions; filt++){
			//first determine which filter to load from and where
			int filterLoc = waveLoc + filt * fftLength;//this is where we should load from for the filter information
			//load in the filter data point
			ComplexD inputFilt = filters[filterLoc];
			//do the math
			temp.x = inputWave.x * inputFilt.x - inputWave.y * inputFilt.y;
			temp.y = inputWave.x * inputFilt.y + inputWave.y * inputFilt.x;
			//now determine where the result should go
			int outputLoc = waveNum * fftLength * numFitFunctions + filt * fftLength + waveLoc;
			//write the result
			output[outputLoc] = temp;	
		}
	}//otherwise do nothing
}

__global__ void multiplication(double* a, double *b, double *c, int len, int totlen){
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < totlen){
		c[i] = a[i] * b[i%len];
	}
}


//these do the complex multiplication
__global__ void multiplication(ComplexD *data, ComplexD *filter, int waveLen, int totalLen) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < totalLen) {
		int filterLoc = i%waveLen;
		ComplexD temp;
		temp.x  = data[i].x * filter[filterLoc].x - data[i].y * filter[filterLoc].y;
		temp.y  = data[i].x * filter[filterLoc].y + data[i].y * filter[filterLoc].x;
		data[i] = temp;
	}
}

//---------------------------------------------------------
//this function grabs the appropriate sections of the waveforms
//---------------------------------------------------------
__global__ void grabRelevantWaveformPieces(double* outputWaves, double *inputWaves, int *locs, int wavelength, int searchRange, int searchLen, int fitLength, int fullRange){
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if(tid < fullRange){
		//figure out what waveform it's looking at
		int wave = tid / fitLength;
		int loc = tid - wave * fitLength;
		int startLoc = locs[wave] - fitLength - searchRange + 1;
		for(int i = 0; i < searchLen; i++){
			int loadLoc = wave * wavelength + startLoc + loc + i;
			double val = inputWaves[loadLoc];
			outputWaves[wave * fitLength * searchLen + fitLength * i + loc] = val;
		}
	}
}

__global__ void grabRelevantWaveformPiecesForFit(double *outputWaves, double *inputWaves, float *locs, int wavelength, int fitLength, int shift, int fullRange){
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if(tid < fullRange){
		int wave = tid / fitLength; //the waveform we are looking at
		int loc = tid - wave * fitLength; //the location in the output we are looking at
		int readLoc = wave * wavelength + int(roundf(locs[wave])) - shift + loc;
		outputWaves[tid] = inputWaves[readLoc];
	}
}

//---------------------------------------------------------
//this function goes through and grabs the relevant YTY values for the eventual calculation of the chi squares
//---------------------------------------------------------
__global__ void grabRelevantElementsYTY(double* outputData, double *inputData, int *locs, int waveLength, int searchRange, int lenToSearch, int fullRange){
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if(tid < fullRange){
		int wave = tid / lenToSearch;
		int searchNum = tid - wave * lenToSearch;
		int searchShift = searchNum - searchRange + locs[wave];
		outputData[wave * lenToSearch + searchNum] = inputData[wave * waveLength + searchShift];
	}
}

//----------------------------------------------
//This function goes and grabs the important fit parameters based on the maximum location
//that was found earlier
//---------------------------------------------
__global__ void grabRelevantElementsX(double* outputParams, double* inputParams, int *locs, int wavelength, int searchRange, int lenToSearch, int numFitFuncs, int fullRange){
	//have one thread per t0 per waveform per basis function
	//innermost values are the fit function values themselves
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id < fullRange){
		int filterID = id % numFitFuncs; //which filter we are looking at
		int waveID = id / (lenToSearch * numFitFuncs); //which waveform we are looking at
		int searchIndex = (id / numFitFuncs) % lenToSearch - searchRange;
		int searchCenter = locs[waveID];
		int inputLocation = waveID * numFitFuncs * wavelength + filterID * wavelength + searchCenter + searchIndex;
		outputParams[id] = inputParams[inputLocation];
		//outputs the fit parameters: output[wave][searchIndex][filter]
	}
}

//grab relevant chi squared values for fitting
__global__ void grabRelevantElementsChiSquared(double* output, int *outputLoc, double *input, float *inputLocs, int searchLen){
	int wave = blockIdx.x;
	int piece = threadIdx.x;
	int quadFitLen = blockDim.x;
	//no if statement required, this is always called with the exact number required
	int shift = quadFitLen / 2;
	int firstElement = int(inputLocs[wave]) - shift;
	//make sure to always have space to grab the right values
	if(firstElement < 0)
		firstElement = 0;
	else if(firstElement + quadFitLen  >= searchLen)
		firstElement = searchLen - quadFitLen - 1;
	int loadLoc = wave * searchLen + firstElement + piece;
	output[wave * quadFitLen + piece] = input[loadLoc];
	if(piece == 0)
		outputLoc[wave] = firstElement;
}


//------------------------------------
// Calculate chi squared values
//------------------------------------
//this function takes input from the output of calculateDiagonalElements
//and the waveform squared. It calculates the non-normalized chi-squared
//from this.
__global__ void calculateChiSquared(double *output, double *yTy, double *xATAx, int totalNumberDataPoints){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if (id < totalNumberDataPoints){
		output[id] = yTy[id] - xATAx[id];
	}
}

__global__ void noSearchGrabValues(double *outputMin, float *outputMinLoc, double *outputParams, double *inputMin, int *inputMinLoc, double *inputParams, int numFilters, int filterLen, int pretrigger, int padWavelength, int batchSize){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id < batchSize){
		int inputLoc = inputMinLoc[id];
		outputMin[id] = inputMin[id]/(float(filterLen)-float(numFilters));
		outputMinLoc[id] = inputLoc - filterLen + pretrigger + 1;
		for(int i = 0; i < numFilters; i++){
			outputParams[id * numFilters + i] = inputParams[id * padWavelength * numFilters + i * padWavelength + inputLoc];
		}
	}
}


//-------------------------------------
// find the sum of the ideal fit parameters
//-------------------------------------
//this function adds the fit parameters as a function of time
//this is used to find the maximum location of the ideal fit parameters
//which is then used to find the minimum chi squared value
__global__ void calculateSumIdealFitParameters(double *output, double *fitParameters, int numIdealFitFunctions, int numFitFunctions, int padWavelength, int batchSize, int totalNumberDataPoints){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if (id < totalNumberDataPoints){
		double temp = 0.0;//temp storage location
		int waveLoc = id % padWavelength; //where in the waveform we are
		int waveNum = id / padWavelength;//what waveform we are in
		int waveShift = waveNum * padWavelength * numFitFunctions;//shifts to beginning of waveform area
		int pointLoc;
		for( int filt = 0; filt < numIdealFitFunctions; filt++){
			pointLoc = waveShift + filt * padWavelength + waveLoc;
			temp += fitParameters[pointLoc];
		}
		//here we transpose the output, this is done to help the finding of the maximum value function that is called after this
		//normally the output is waveform[0][:], waveform[1][:], waveform[2][:], etc
		//now we want waveform[0][0], waveform[1][0], waveform[2][0] etc
		//in python notation that is
		int outputLoc = waveNum + waveLoc * batchSize;
		output[outputLoc] = temp;
	}
}

//-------------------------------------
//Find Max Value
//-------------------------------------
//Note that this is a very slow max finding function
//this should absolutely be optimized if possible
__global__ void findMaxLocIdealFitParameter(int *maxValLocs, double *fitParameterSum, int fitFunctionLength, int padWavelength, int shiftVal, int batchSize, int searchRange, int totThreads){
	int id = threadIdx.x + blockIdx.x * blockDim.x;	
	if (id < totThreads){
		//each thread handles it's own minima
		//to keep the memory access' strided, the data has been transposed
		float maxLoc = 0;
		float maxVal = -99999;
		//note that we only search in the range inside of the proper convolution zone
		//this means starting fitFunctionLength in and ending at padWavelength - fitFunctionLength
		for(int i = batchSize * fitFunctionLength + id; i < batchSize * (padWavelength-fitFunctionLength); i+= batchSize){
			float testVal = fitParameterSum[i];
			if(testVal > maxVal){
				maxVal = testVal;
				maxLoc = i/batchSize;
			}
		}
		//keep the maximum as the max location
		//make sure the max value isn't too close to either end of the waveform
		if(maxLoc - fitFunctionLength < 0)//if it's too close to the end, shift it to be okay 
			maxLoc = fitFunctionLength;
		else if(maxLoc + fitFunctionLength > padWavelength)//if it's too close to the other end make it okay
			maxLoc = padWavelength - fitFunctionLength;
		maxValLocs[id] = maxLoc;
	}
}


//------------------------------------
//this function finds the local chi squared minimum
//------------------------------------
__global__ void localChiSquaredMinSearch(double *outputMins, float* outputMinLocs, double *outputParams, double *chiSquared, int *maxLocs, double *inputParams, int searchRange, int searchLen, int fitFunctionLength, int pretrigger, int padWavelength, int numFilters, int batchSize){
	//one thread for each waveform
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id < batchSize){
		//now in this case a search actually has to be done
		int shift = id * searchLen; //the starting location
		float min = chiSquared[shift]; //the first chi squared value
		int minLoc = 0;
		for(int i = 0; i < searchLen; i++){	
			float temp = chiSquared[i+shift];
			if(temp < min){
				min = temp;
				minLoc = i;
			}
		}
		outputMins[id] = min/(float(fitFunctionLength)-float(numFilters));
		int trueLocation = minLoc + maxLocs[id] - searchRange;//this is the location in the full output
		//use this for the output fit parameters
		for(int i = 0; i < numFilters; i++){
			outputParams[id * numFilters + i] = inputParams[id * numFilters * padWavelength + i * padWavelength + trueLocation];
		}
		outputMinLocs[id] = minLoc + maxLocs[id] - fitFunctionLength + pretrigger + 1 - searchRange;
	}
}	

//this function goes through and does the minimization just like the previous function does
//it also grabs all the relevant values around the minima
__global__ void chiMinSearchAndGrab(double *relevantChis, double *inputChis, float *startLocs, int searchRange, int searchLen, int quadFitLen, int batchSize){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id < batchSize){
		int shift = id * searchLen; //the starting location
		float min = inputChis[shift]; //the first chi squared value
		int minLoc = 0;
		for(int i = 0; i < searchLen; i++){	
			float temp = inputChis[i+shift];
			if(temp < min){
				min = temp;
				minLoc = i;
			}
		}
		//make sure the output range is entirely valid first
		int fitRange = int(quadFitLen/2);
		int startLoc = minLoc - fitRange;
		if(startLoc < 0)
			startLoc = 0;
		else if(startLoc + quadFitLen > searchLen)
			startLoc = searchLen - 1 - quadFitLen;
		//now do the output
		shift = startLoc + id * searchLen;
		for(int i = 0; i < quadFitLen; i++){
			relevantChis[i + id * quadFitLen] = inputChis[i + shift];
		}
		startLocs[id] = startLoc;
	}
}

//use the fit parameter results to find the minimum in the quadratic function
__global__ void calculateChiFitMin(double *minChis, float *minChiLocs, double *fitParams, int *maxLocs, double *outputParams, double* inputParams, int searchRange, int searchLen, int quadFitLen, int numFitFuncs, int fitFunctionLength, int pretrigger, int batchSize){
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < batchSize){//load in the fit parameters
		float a, b, c;
		a = fitParams[i * 3];//constant
		b = fitParams[i * 3 + 1];//linear
		c = fitParams[i * 3 + 2];//quadratic

		//calculate minimum
		float minLoc = - b / (2.0f * c);
		float minVal = a + minLoc * b + minLoc * minLoc * c;
		//starting location of the fits
		int startLoc = minChiLocs[i];
		if(minLoc < 0 || minLoc > quadFitLen){
			//do nothing, bad fit result so just trust the lowest value
			minLoc = float(startLoc + int(quadFitLen/2));
		}
		else{
			minLoc += float(startLoc);
		}
		//at this point minLoc is the position in the chi squared values that were calculated and determined
		int paramLoc = int(round(minLoc));
		//make sure it's within the proper range
		if(paramLoc == searchLen)
			paramLoc = searchLen - 1;
		else if(paramLoc < 0)
			paramLoc = 0;
		//use the relevant fit parameters to output those results now
		for(int j = 0; j < numFitFuncs; j++){
			outputParams[j + i * numFitFuncs] = inputParams[i * numFitFuncs * searchLen + paramLoc * numFitFuncs + j];
		}
		minChis[i] = minVal;
		minChiLocs[i] = minLoc + float(maxLocs[i] - searchRange - fitFunctionLength + pretrigger + 1.0f);
	}
}

//this function goes through and using the final chi squared minima location 
//determines the proper fit parameter to output
__global__ void grabResultFitParameters(double *outParams, double *inParams, double *chiSquaredMin, int padWavelength, int fitFunctionLength, int numFitFuncs){
	int wave = blockIdx.x;
	int filt = threadIdx.x;
	//only call this with exactly as many blocks and threads as we need
	//round this to the nearest integer t0 value
	int t0 = roundf(chiSquaredMin[wave]);
	//with that information go through and grab the proper fit parameters
	int shift = t0 + fitFunctionLength/2;
	outParams[wave * numFitFuncs + filt] = inParams[wave * padWavelength * numFitFuncs + filt * padWavelength + shift];
}


//this function simply shifts the minimum location within the chi squared array to representing the 
//global minimum location
__global__ void shiftMinChiSquaredValue(double *chiMinLocs, double *maxFitLocs, int searchRange, int num){
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < num)
		chiMinLocs[i] = chiMinLocs[i] + maxFitLocs[i] - searchRange;
}

__global__ void generateIdealWaveforms(double *simulatedWave, double *fitParameters, double* fitFunctions, int numIdealPulses, int numFiltersTotal, int padWavelength, int totalNum){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if (id < totalNum){
		int waveID = id / padWavelength;
		int waveLoc = id - waveID * padWavelength;
		//shift the output of this so that there are at least trapFilterLen values before and after the 
		//fake waveform itself
		float fakePoint = 0.0f;
		//only change this value if it's in the proper range
		//determine where in each filter we are
		for(int filt = 0; filt < numIdealPulses; filt++){
			fakePoint += float(fitParameters[waveID * numFiltersTotal + filt] * fitFunctions[filt * padWavelength + waveLoc]);
		}
		simulatedWave[id] = fakePoint;
	}
}

//-------------------------------------------------------
//Compare the different chi squared values for the PSD
//-------------------------------------------------------
__global__ void gpuPSD(int *predictions, float *percents, double *chis, int numFitFuncs, int batchSize){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id < batchSize){
		int minloc = 0;
		double min = 1.0E300;
		double secondMin = 1.0E300;
		for(int i = 0; i < numFitFuncs;i++){
			double temp = chis[id + i*batchSize];
			if(temp < min){
				secondMin = min;
				min = temp;
				minloc = i;
			}
			else if(temp < secondMin){
				secondMin = temp;
			}
		}
		predictions[id] = minloc;
		percents[id] = (secondMin - min)/(min);
	}
}

//-------------------------------------
//addAllBasisFunctions
//-------------------------------------
//thihs is basically the same thing as generateIdealWaveforms
//except it expects the ideal pulses to already be in the 
//output waveform and simply adds the baseline functions
__global__ void addAllBasisFunctions(float *simulatedWave, float *fitParameters, float* fitFunctions, int numIdealPulses, int numFiltersTotal, int padWavelength, int totalNum){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if (id < totalNum){
		//setup the storage for the results
		float fakePoint = simulatedWave[id];
		//determine where in each filter we are
		int waveIndex = id % padWavelength;
		int waveNumber = id / padWavelength;
		for(int filt = numIdealPulses; filt < numFiltersTotal; filt++){
			fakePoint += fitParameters[waveNumber * numFiltersTotal + filt] * fitFunctions[filt * padWavelength + waveIndex];
		simulatedWave[id] = fakePoint;
		}
	}
}

//-------------------------------------
// Calculate Residuals
//-------------------------------------
__global__ void determineResidualsSquared(float *residuals, float *originalWave, float *minChiLocs, int fitFunctionLength, int padWavelength, int totalNumPoints){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id < totalNumPoints){
		//figure out what waveform we are in
		int waveID = id / fitFunctionLength;
		int waveLoc = id % fitFunctionLength;
		//load in the fit waveform value
		int outputLoc = waveID * padWavelength + waveLoc;
		float fitVal = residuals[outputLoc];
		//determine the minimum chi location for the time shift
		int timeShift = minChiLocs[waveID] - fitFunctionLength;
		//now load in the value from the original waveform
		//make sure to properly shift in time
		float waveVal = originalWave[outputLoc + timeShift];
		//now output the results
		residuals[outputLoc] = powf(fitVal - waveVal,2.0f);
	}
}

//------------------------------------
//find sigma value and normalize the chi squared with it
//------------------------------------
__global__ void findSigmaAndNormalizeChiSquared(float *chiSquared, float *residuals, int fitFunctionLength, int numFitFunctions, int padWavelength, int batchSize){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	if(id < batchSize){
		float mean = 0.0f;
		//id determines our waveform
		int waveShift = id * padWavelength;
		for(int i = waveShift; i < waveShift + fitFunctionLength; i++){
			mean+=residuals[i];
		}
		mean /= float(fitFunctionLength);			
		//with the mean known, this is the sigma value
		//now use this to normalize chi squared
		chiSquared[id] = chiSquared[id] / float(((fitFunctionLength - numFitFunctions) * mean));			
	}
}



//-------------------------------------
// 		Type Conversions
//-------------------------------------
__global__ void typeConversion(short* input, float* output, int len){
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < len) {
		output[i] = (float)input[i];
	}
}

__global__ void typeConversion(short* input, double* output, int len){
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < len) {
		output[i] = (double)input[i];
	}
}

__global__ void typeConversion(double *input, float *output, int len){
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < len){
		output[i] = float(input[i]);
	}
}


//-------------------------------------
//			andFunction
//-------------------------------------
__global__ void andFunction(short* data, int len) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < len) {
		short temp = data[i];
		temp       = temp & 16383;
		temp      -= (temp / 8192) * 16384;
		data[i]    = temp;	
	}
}

__global__ void andFunctionPad(short *data, int waveLen, int padLen, int totalLen){
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < totalLen){
		//make sure the thread has a data point first
		//now determine if the thread is on a normal data point, or a zero
		int waveLoc = i % padLen;
		if (waveLoc < waveLen){//if its in the normal waveform, then do normal and function to it
			short temp = data[i];
			temp = temp & 16383;
			temp -= (temp >> 13) << 14;
			data[i] = temp;
		}
		else{//otherwise its a padded value and it should be zero
			data[i] = 0;
		}
	}
}

//-------------------------------------
//			averagesFind
//-------------------------------------
__global__ void averagesFind(const short *data, float *avgs, int np, int numwaves, int offset) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	if(i < numwaves) {
		float avg        = 0.0f;
		int shift      = np * i;
		for(int j = 0; j < offset; j++)
			avg += data[j+shift];
		avgs[i] = avg / float(offset);
	}
}

//-------------------------------------
//		Boxcar Filter Definition
//-------------------------------------
__global__ void defineBoxcarFilter(float *filter, int numOnes, int len){
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < len){
		if(i < numOnes)
			filter[i] = 1.0f/(float(len));
		else
			filter[i] = 0.0f;
	}
}

//-------------------------------------
//		baselineShift (overload x2)
//-------------------------------------
// When data is short
__global__ void baselineShift(short *data, const float *avgs, int np, int len) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < len) {
		int index = i / np;
		int avg   = avgs[index];
		data[i]  -= avg; 
	}
}
// When data is float
__global__ void baselineShift(float *data, const int *avgs, int np, int len) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < len) {
		int index = i / np;
		int avg   = avgs[index];
		data[i]  -= avg; 
	}
}

//-------------------------------------
//    baselineShiftShortToFloat
//-------------------------------------
// Combination of baselineShift and short to float methods
__global__ void baselineShiftShortToFloat(const short *inData, float *outData, 
		const float *avgs, int padnp, int np, int len) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < len) {
		int waveloc = i % padnp;
		if(waveloc<np){
			int index  = i / padnp;
			float avg    = avgs[index];
			float temp = inData[i];
			temp      -= avg;
			outData[i] = temp;
		}
		else{
			outData[i]=0.0f;
		}
	}
} 

//-------------------------------------
//knownLocEnergyT0
//-------------------------------------
__global__ void knownEnergyT0(const double *data, float *result, int np, int shift, int numWaves){
	int i = threadIdx.x + blockIdx.x * blockDim.x;//this is the waveform number
	if(i < numWaves){
		int waveloc = i * np + shift; //where to read out the energy
		result[i] = float(data[waveloc]);
	}
}

//-------------------------------------
//			energyT0
//-------------------------------------
//		by David Mathews
//-------------------------------------
__global__ void energyT0(const float *data, Complex *result, int spacing, int np, int rise, int top, int numWaves, float thresholdPercent){ 
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < numWaves) {
		float leftCross=0.0,rightCross=0.0,maxVal=0.0;
		int maxLoc=0;
		int waveLoc=i*spacing;
		// find maximum
		float testVal=0.0;
		for (int j = 0; j < np; j++){
			testVal=data[waveLoc+j];
			if (testVal > maxVal) {
				maxVal=testVal;
				maxLoc=j;
			}
		}	
		//if(i == 0){
		//	printf("%f : %d\n", maxVal, maxLoc);
		//}
		float threshold = thresholdPercent * maxVal;
		// find both crossings
		int t0shift=rise+0.5*float(top);
		int j = maxLoc;
		while(j>maxLoc-t0shift && j > 1){
			if (data[waveLoc+j]>=threshold && data[waveLoc+j-1]<=threshold){
				leftCross=j-0.5;
				j=0;	
			} 		
			j--;
		}
		j=maxLoc;
		while(j<maxLoc+t0shift && j<np-1){
			if(data[waveLoc+j]>=threshold && data[waveLoc+j+1]<=threshold){
				rightCross=j+0.5;
				j=maxLoc+t0shift;
			}
			j++;
		}
		int midPoint=(rightCross+leftCross)/2.0f;
		//if(i == 0)
		//	printf("%f %f %f\n", leftCross, rightCross, midPoint);
		// calculate the mean of the 5 points to the right of the middle
		float mean=0.0;
		for(j=midPoint+waveLoc;j<midPoint+waveLoc+5;j++)
			mean+=data[j];
		mean=mean/5.0f;
		//output energy as the x coordinate, t0 as the y coordinate
		result[i].x= mean;
		result[i].y= midPoint - t0shift; // subtract off rise and top/2 later
	}
}
//this is the same thing as the last function
//except it only gets the energy and not t0
__global__ void energyT0(const float *data, float *result, int spacing, int np, int rise, int top, int numWaves, float thresholdPercent){ 
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < numWaves) {
		float leftCross=0.0,rightCross=0.0,maxVal=0.0;
		int maxLoc=0;
		int waveLoc=i*spacing;
		// find maximum
		float testVal=0.0;
		for (int j = 0; j < np; j++){
			testVal=data[waveLoc+j];
			if (testVal > maxVal) {
				maxVal=testVal;
				maxLoc=j;
			}
		}	
		//if(i == 0){
		//	printf("%f : %d\n", maxVal, maxLoc);
		//}
		float threshold = thresholdPercent * maxVal;
		// find both crossings
		int t0shift=rise+0.5*float(top);
		int j = maxLoc;
		while(j>maxLoc-t0shift && j > 1){
			if (data[waveLoc+j]>=threshold && data[waveLoc+j-1]<=threshold){
				leftCross=j-0.5;
				j=0;	
			} 		
			j--;
		}
		j=maxLoc;
		while(j<maxLoc+t0shift && j<np-1){
			if(data[waveLoc+j]>=threshold && data[waveLoc+j+1]<=threshold){
				rightCross=j+0.5;
				j=maxLoc+t0shift;
			}
			j++;
		}
		int midPoint=(rightCross+leftCross)/2.0f;
		//if(i == 0)
		//	printf("%f %f %f\n", leftCross, rightCross, midPoint);
		// calculate the mean of the 5 points to the right of the middle
		float mean=0.0;
		for(j=midPoint+waveLoc;j<midPoint+waveLoc+5;j++)
			mean+=data[j];
		mean=mean/5.0f;
		//output energy as the x coordinate, t0 as the y coordinate
		result[i]= mean;
	}
}
__host__ __device__ inline int getbit64(superint cont, int bitnum){
	return ((cont>>(63-bitnum))&1);
}

__global__ void gpuDecompression(short *waves, const unsigned int *compwaves, const int *complengths, const short *firsts, int wavelen, int paddedLength, int MAXCOMPLEN, int batchsize){
	int wave = threadIdx.x + blockIdx.x * blockDim.x;
  if(wave<batchsize){
    int waveshift = wave * paddedLength;
    int compshift = wave * MAXCOMPLEN;
    int complen = complengths[wave];
    //short m = 8;
    short firstval = firsts[wave];

    superint read=0;//buffers read in to
    int bit=0;//bit you are currently reading
    int q=0,r=0,sign=0;//q=quotient, r=remainder,sign is the sign of the number, t=?
    int bitloc=0;//location in read that we are working on

    read=compwaves[compshift];
    read=read<<32;
    read=read|(compwaves[compshift + 1]);

    short currentVal = 0;
    waves[waveshift] = firstval;
    for(int i=1;i<wavelen;i++){
      q=0;//
      while(!getbit64(read,bit)){
        q++;
        bit++;
      }
      bit++;//gets the extra 1 ending the number
      if(q==8){
        currentVal=(read>>(48-bit))&32767; //48 because of 64 - 16,32767=2**15-1
        unsigned int zero = currentVal >> 14; //will be 0 for values <16384, >=16384 -> 1
        currentVal = currentVal - zero*32768; //should be the same as subtracting 32768 
        bit+=16;//how many bits we basically just read
      }
      else{
        //now get the sign, its just the next bit in the reversed array
        sign=-(getbit64(read,bit)<<1);//this is either a 0 or 1, doing this allows for subtraction later
        bit++;
        //now read in the remainder
        r=0;
				//r=(read>>(64-rshift-bit))&rshifthigh;
        r = (read>>(61-bit))&7;
        bit+=3;
        //now store that information
        currentVal=(q<<3)+r;
        currentVal=currentVal+sign*currentVal;//this is why we did that negative two multiplication before
        //doing this removes an extra if statement
      }
      firstval = currentVal + firstval;
      waves[i+waveshift]=firstval;
      //now that the bit has been read in, check to see if we are in the next storage location
      if(bit>=32){
        bit=bit-32;
        bitloc++;//shift down one
        read=compwaves[bitloc+compshift];
        read=read<<32;
        if(bitloc!=complen-1){//if we aren't at the end do this
          read=read|compwaves[bitloc+1+compshift];
        }//if we are at the end, do nothing
      }
    }
  }

}



}
