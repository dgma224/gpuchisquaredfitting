//params.cu
#include "params.h"
#include "general_functions.h"
#include <math.h>
namespace gpuAnalysis {

const int NUM_THREADS_STD = 1024, NUM_THREADS_BASE = 32;

//--------------------------------
// 		Constructor
//--------------------------------
PARAMS::PARAMS() {
	//open parameter file
	// Defaults already set above
	std::ifstream pfin ("param.dat");
	std::string tempFileFormat;
	if(pfin.is_open()){
		pfin >> batchSize >> searchRange >> quadFitLen >> filterFile >> tempFileFormat >> gpusToUse;
		if(searchRange * 2 + 1 < quadFitLen){
			std::cerr<<"invalid input parameters: searchRange * 2 + 1 ("<<searchRange * 2 + 1<<") must be >= quadFitLen ("<<quadFitLen<<")"<<std::endl;
			exit(1);
		}
	}
	else{
		std::cout << "param.dat failed to open: Code Exiting" << std::endl;
		exit(1);
	}
	if(tempFileFormat == "ca45")
		fileFormat = -2;
	else if(tempFileFormat == "oldnab")
		fileFormat = -1;
	else if(tempFileFormat == "nab"){
		fileFormat = 0;
	}
	else{
		std::cout<<"file format not recognized"<<std::endl;
		exit(1);
	}
	pfin.close();
	searchLen = searchRange * 2 + 1;
}


void PARAMS::getDataFiles(std::string data_location) {
	//open the data file location manager, expects file locations in dataloc.dat
	std::ifstream fin (data_location);
	std::string line;
	if(fin.is_open()) {
		for(std::string i; fin >> i; )
			datafiles.push_back(i);
	}
	else {
		std::cout << "Data Input File failed to open" << std::endl;
		exit(1);
	}    
	fin.close();
}

void PARAMS::getFileInformation(std::string filename) {
	FILE* fin = fopen(filename.c_str(), "rb");
  //get length of data file and determine number of waveforms
	int nabversion, headerlength;//define them here so the header length information is known
  if(!fin) {
    perror("Halp");
    std::cerr << filename  << " couldn't be opened: Code Exiting\n";
    exit(1);
  }
	//depending on what type of file format we are dealing with, different things are done here
	if(fileFormat == -2)//this is the ca45 format
		fseek(fin, 8, SEEK_SET);
	else if(fileFormat == -1){//old nab format, nothing needs to be done
	}
	else{//new nab format
		fread(&nabversion, sizeof(int), 1, fin); 
		fread(&headerlength, sizeof(int), 1, fin);//do this to learn how large the header is
		if(nabversion == 0){//just past the header now	
			fseek(fin, 8 + headerlength, SEEK_SET);
		}
		else{
			std::cerr<<"unrecognized Nab file format: "<<nabversion<<std::endl;
			std::cerr<<"code exiting"<<std::endl;
			exit(1);
		}
	}
	WAVEFORM header = readHead(fin, fileFormat);
	wavelength = header.length;
	//now that this works for both compressed and decompressed files so its okay to do this here		
	fseek(fin, 0L, SEEK_END);
	long int size = ftell(fin);
	fseek(fin, 0L, SEEK_SET);
	numWaves=0;
	//now check to see if the file is compressed
	if(filename.compare(filename.size()-5,5,".comp") == 0){
		isCompressed = true;
		bool found = false;
		maxCompLen = 0;
		long loc = 0.0;	
		while(found==false){
			WAVEFORM header=readHeadComp(fin);
			if(header.complength>maxCompLen)
				maxCompLen=header.complength;
			loc+=sizeof(int)*header.complength+31;
			fseek(fin,loc,SEEK_SET);
			numWaves++;
			long temp=ftell(fin);
			if(temp>=size)
				found=true;
		}
	}
	else{
		isCompressed = false;
		//if this is the case all waveforms are the same length, much easier to determine
		//the number of waveforms
		if(fileFormat == -2){//ca45 format
			numWaves = int(((double)size - 8)/(33 + wavelength * 2));
		}
		if(fileFormat == -1){ //old nab
      numWaves = int(((double)size) / (25 + wavelength * 2));
    }
    else{//current nab
      numWaves = int(((double)size - 8 - headerlength) / (33 + wavelength * 2));
    }
	}
	fclose(fin);
}
//this is basically the replacement for the vals struct
//since params is updated for each file now its equivalent in functionality
void PARAMS::determineSizes(){
	//sizes for variables that have one instance per waveform
	shortSize = sizeof(short) * batchSize;
	intSize = sizeof(int) * batchSize;
	floatSize = sizeof(float) * batchSize;
	doubleSize = sizeof(double) * batchSize;
	complexSize = sizeof(float2) * batchSize;
	complexDoubleSize = sizeof(double2) * batchSize;

	//sizes for batches
	shortSizeBatch = shortSize * padWavelength;
	intSizeBatch = intSize * padWavelength;
	floatSizeBatch = floatSize * padWavelength;
	doubleSizeBatch = doubleSize * padWavelength;
	complexSizeBatch = complexSize * padWavelength;
	complexDoubleSizeBatch = complexDoubleSize * padWavelength;

	//fft sizes
	complexHalfSizeBatch = sizeof(float2) * paddedFFTLength * batchSize;
	complexDoubleHalfSizeBatch = sizeof(double2) * paddedFFTLength * batchSize;

	intSizeComp = sizeof(int) * batchSize * maxCompLen;
  
	//now determine the blocks and threads
	mulThreads = andThreads = sumThreads = subThreads = searchThreads = NUM_THREADS_STD;
	baseThreads = NUM_THREADS_BASE;

	mulBlocks  = calcNumBlocks(batchSize * paddedFFTLength, mulThreads);
  andBlocks  = calcNumBlocks(batchSize * padWavelength, andThreads);
  baseBlocks = calcNumBlocks(batchSize,  baseThreads);
  sumBlocks  = calcNumBlocks(batchSize, sumThreads);
  subBlocks  = calcNumBlocks(batchSize * wavelength, subThreads);
	searchBlocks = calcNumBlocks(batchSize * searchLen, baseThreads);
	//now determine the number of batches and that information
	numBatches = numWaves / batchSize;
	lastBatch = (numWaves - floor(numBatches/2) * 2 * batchSize) / batchSize;
	leftover = numWaves - numBatches * batchSize;
	//update the scale parameter with the length of the waveform information
	trapScaleFloat = trapScaleFloat /((float)padWavelength);			
	trapScaleDouble = trapScaleDouble / ((double)padWavelength);
}

//define the copy constructor
PARAMS::PARAMS(const PARAMS& p) {
	batchSize = p.batchSize;
	top = p.top;
	rise = p.rise;
	wavelength = p.wavelength;
	searchRange = p.searchRange;
	quadFitLen = p.quadFitLen;
	searchLen = p.searchLen;
	padWavelength = p.padWavelength;
	paddedFFTLength = p.paddedFFTLength;
	numBatches = p.numBatches;
	lastBatch = p.lastBatch;
	leftover = p.leftover;
	fileFormat = p.fileFormat;
	gpusToUse = p.gpusToUse;
	tau = p.tau;
	percentThreshold = p.percentThreshold;
	trapScaleFloat = p.trapScaleFloat;
	trapScaleDouble = p.trapScaleDouble;
	trapLen = p.trapLen;
	fftNormalization = p.fftNormalization;
	isCompressed = p.isCompressed;
	maxCompLen = p.maxCompLen;
	numWaves = p.numWaves;
	for(int i = 0; i < p.datafiles.size(); i++){
		datafiles.push_back(p.datafiles[i]);
	}
	filterFile = p.filterFile;
	shortSize = p.shortSize;
	shortSizeBatch = p.shortSizeBatch;
	intSize = p.intSize;
	intSizeBatch = p.intSizeBatch;
	intSizeComp = p.intSizeComp;
	floatSize = p.floatSize;
	floatSizeBatch = p.floatSizeBatch;
	doubleSize = p.doubleSize;
	doubleSizeBatch = p.doubleSizeBatch;
	complexSize = p.complexSize;
	complexHalfSizeBatch = p.complexHalfSizeBatch;
	complexSizeBatch = p.complexSizeBatch;
	complexDoubleSize = p.complexDoubleSize;
	complexDoubleHalfSizeBatch = p.complexDoubleHalfSizeBatch;
	complexDoubleSizeBatch = p.complexDoubleSizeBatch;
	mulThreads = p.mulThreads;
	mulBlocks = p.mulBlocks;
	andThreads = p.andThreads;
	andBlocks = p.andBlocks;
	baseThreads = p.baseThreads;
	baseBlocks = p.baseBlocks;
	sumThreads = p.sumThreads;
	sumBlocks = p.sumBlocks;
	subThreads = p.subThreads;
	subBlocks = p.subBlocks;
	searchThreads = p.searchThreads;
	searchBlocks = p.searchBlocks;
}


}  // Namespace gpuAnalysis
