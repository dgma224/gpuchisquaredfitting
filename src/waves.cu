//waves.cu
#include "filters.h"
#include "waves.h"

namespace gpuAnalysis {
//------------------------------------
// 			 Contructor
//-----------------------------------
WAVES::WAVES(PARAMS &param, FILTERS &filters) {
	weighting = filters.weighting;
	psdWeight = filters.psdWeight;
	numIdealPulses = filters.numIdealPulses;
	//things that are always one precision or another
	//read in waveform storage
	checkCudaErrors(cudaMallocHost(&waveforms,param.shortSizeBatch));
	checkCudaErrors(cudaMalloc(&d_waveforms, param.shortSizeBatch));
	
	//maximum fit location since those are also always integer values
	checkCudaErrors(cudaMalloc(&d_maxFitLocs, param.intSize));

	//minimum t0 location storage since those are always floating point
	checkCudaErrors(cudaMallocHost(&minChiSquaredLoc, param.doubleSize));
	checkCudaErrors(cudaMalloc(&d_minChiSquaredLoc, param.doubleSize));
	
	if(filters.numIdealPulses > 1){
		checkCudaErrors(cudaMalloc(&d_relevantFitPieces, sizeof(double) * param.batchSize * filters.fitFunctionLength));
		if(psdWeight > 0){
			checkCudaErrors(cudaMalloc(&d_psdTempYTY, sizeof(double) * param.batchSize * filters.fitFunctionLength));
		}
		checkCudaErrors(cudaMalloc(&d_PSDChis, param.doubleSize * filters.numIdealPulses));
		checkCudaErrors(cudaMalloc(&d_psdYTY, param.doubleSize));
		checkCudaErrors(cudaMalloc(&d_psdATA, param.doubleSize));
		checkCudaErrors(cudaMalloc(&d_psdParams, param.doubleSize * (filters.numFitFunctions - filters.numIdealPulses + 1)));
		checkCudaErrors(cudaMalloc(&d_psdTempATA, param.doubleSize * (filters.numFitFunctions - filters.numIdealPulses + 1)));
		psdPredictions = (int*)malloc(param.intSize);
		checkCudaErrors(cudaMalloc(&d_psdPredictions, param.intSize));
		percents = (float*)malloc(param.floatSize);
		checkCudaErrors(cudaMalloc(&d_percents, param.floatSize));
		
	}


	//trap filter results
	checkCudaErrors(cudaMallocHost(&trapResults, param.floatSize));
	checkCudaErrors(cudaMalloc(&d_trapResults, param.floatSize));	
	checkCudaErrors(cudaMalloc(&d_trapResultsDouble, param.doubleSize));
	//fit results storage for hte single precision, no matter what we need these
	checkCudaErrors(cudaMallocHost(&fitResults, param.batchSize * filters.numFitFunctions * sizeof(double)));
	checkCudaErrors(cudaMalloc(&d_fitResults, param.batchSize * filters.numFitFunctions * sizeof(double)));
		
	//chi squared results storage, again no matter what these are needed for data output
	checkCudaErrors(cudaMallocHost(&minChiSquared, param.doubleSize));
	checkCudaErrors(cudaMalloc(&d_minChiSquared, param.doubleSize));

	//allocate storage for hte quadratic fit locations
	checkCudaErrors(cudaMalloc(&d_quadFitLocs, param.intSize));

	//plan for doing the forward fourier transform of the waveforms
	//this version is always used due to the trapezoidal filter being applied at the later stages
	checkCudaErrors(cufftPlan1d(&planWavesForward,param.padWavelength,CUFFT_R2C,param.batchSize));
	//plans for doing the smaller inverse fourier transform for the squared waveforms
	//this is also done in both cases
	checkCudaErrors(cufftPlan1d(&planSquaredInverse, param.padWavelength, CUFFT_C2R, param.batchSize));	
	
	//now handle the things that need to be done in one particular precision 
	//waveform storage
	checkCudaErrors(cudaMalloc(&d_doubleWaveforms, param.doubleSizeBatch));
	//fft and temp storage
	checkCudaErrors(cudaMalloc(&d_complexWaveforms, param.complexDoubleHalfSizeBatch));
	checkCudaErrors(cudaMalloc(&d_complexWaveformsCopy, param.complexDoubleHalfSizeBatch * filters.numFitFunctions));
	//fit parameter storage
	checkCudaErrors(cudaMalloc(&d_fitParameters, param.doubleSizeBatch * filters.numFitFunctions));
	//fft fit parameters
	checkCudaErrors(cudaMalloc(&d_fftFitParameters, param.complexDoubleHalfSizeBatch * filters.numFitFunctions));
	//sum of hte fit parameters
	checkCudaErrors(cudaMalloc(&d_sumFitParams, param.doubleSizeBatch));
	//relevant fit parameters
	checkCudaErrors(cudaMalloc(&d_relevantFitParams, param.batchSize * param.searchLen * filters.numFitFunctions * sizeof(double)));
	//now the weighting storage if that's needed
	if(filters.weighting == 2){
		checkCudaErrors(cudaMalloc(&d_relevantWaveformPieces, sizeof(double) * param.batchSize * param.searchLen * filters.fitFunctionLength));
		checkCudaErrors(cudaMalloc(&d_temporaryYTYValues, sizeof(double) * param.batchSize * param.searchLen * filters.fitFunctionLength));
	}
	//relevant YTY values
	checkCudaErrors(cudaMalloc(&d_relevantYTY, param.batchSize * param.searchLen * sizeof(double)));
	//temporary storage for the xATAx values
	checkCudaErrors(cudaMalloc(&d_tempxATAx, param.batchSize * param.searchLen * filters.numFitFunctions * sizeof(double)));
	//storage for the xATAx values after calculation has been done
	checkCudaErrors(cudaMalloc(&d_xATAx, param.batchSize * param.searchLen * sizeof(double)));
	//waveform squared
	checkCudaErrors(cudaMalloc(&d_waveformSquared, param.doubleSizeBatch));
	//fft waveform squared
	checkCudaErrors(cudaMalloc(&d_fftWaveformSquared, param.complexDoubleHalfSizeBatch));
	//chi squared values
	checkCudaErrors(cudaMalloc(&d_chiSquared, param.doubleSizeBatch));
	//fitted chi squared values
	checkCudaErrors(cudaMalloc(&d_fitChiSquared, param.batchSize * param.quadFitLen * sizeof(double)));
	//allocate the chi squared fit parameters storage
	checkCudaErrors(cudaMalloc(&d_chiSquaredFitParams, param.batchSize * 3 * sizeof(double)));
	//fit results storage
	checkCudaErrors(cudaMalloc(&d_fitResults, param.batchSize * filters.numFitFunctions * sizeof(double)));
	//chi squared results storage
	checkCudaErrors(cudaMallocHost(&minChiSquared, param.doubleSize));
	checkCudaErrors(cudaMalloc(&d_minChiSquared, param.doubleSize));
	checkCudaErrors(cudaDeviceSynchronize());
	//same thing but the double precision versions in case those need to be used instead
	//create plans
	checkCudaErrors(cufftPlan1d(&planWavesForward, param.padWavelength, CUFFT_D2Z, param.batchSize));
	checkCudaErrors(cufftPlan1d(&planFitParamsInverse, param.padWavelength,CUFFT_Z2D, param.batchSize * filters.numFitFunctions));
	checkCudaErrors(cufftPlan1d(&planSquaredInverse, param.padWavelength, CUFFT_Z2D, param.batchSize));
	
	//create the cudaStreams
	checkCudaErrors(cudaStreamCreate(&stream));
	//set the cuda streams for the plans
	checkCudaErrors(cufftSetStream(planWavesForward,stream));
	checkCudaErrors(cufftSetStream(planSquaredInverse,stream));
	checkCudaErrors(cufftSetStream(planWavesForward,stream));
	checkCudaErrors(cufftSetStream(planFitParamsInverse,stream));
	checkCudaErrors(cufftSetStream(planSquaredInverse,stream));
	
	//now create the cublas handles and whatnot
	cublasStatus = cublasCreate(&cublasHandle);
	if (cublasStatus != CUBLAS_STATUS_SUCCESS) {
		std::cerr<<"CUBLAS initialization error"<<std::endl;
		exit(1);
	}
	//set the stream of the cublas handle
	cublasStatus = cublasSetStream(cublasHandle, stream);
	if (cublasStatus != CUBLAS_STATUS_SUCCESS) {
		std::cerr<<"CUBLAS assigning stream failed"<<std::endl;
		exit(1);
	}
	//allocate storage for the temporary matrix
	
	if(param.isCompressed){
		//host allocation
		checkCudaErrors(cudaMallocHost(&compwaves, param.intSizeComp));
		checkCudaErrors(cudaMallocHost(&firsts, param.shortSize));
		checkCudaErrors(cudaMallocHost(&complengths, param.shortSize));
	
		checkCudaErrors(cudaMalloc(&d_firsts, param.shortSize));
		checkCudaErrors(cudaMalloc(&d_compwaves, param.intSizeComp));
		checkCudaErrors(cudaMalloc(&d_complengths, param.shortSize));	
	}
	checkCudaErrors(cudaDeviceSynchronize());	
}

//---------------------------------
// 			 Destructor
//---------------------------------
WAVES::~WAVES() {
	//in order of occurence in the allocation
	//things that don't change with precision
	cudaFreeHost(waveforms);
	cudaFree(d_waveforms);
	
	cudaFree(d_maxFitLocs);

	cudaFreeHost(minChiSquaredLoc);
	cudaFree(d_minChiSquaredLoc);

	cudaFreeHost(trapResults);
	cudaFree(d_trapResults);

	cudaFreeHost(fitResults);
	cudaFree(d_fitResults);
	
	cudaFreeHost(minChiSquared);
	cudaFree(d_minChiSquared);
	
	cufftDestroy(planWavesForward);
	cufftDestroy(planSquaredInverse);	

	//double precision stuff
	cudaFree(d_doubleWaveforms);
	cudaFree(d_complexWaveforms);
	cudaFree(d_complexWaveformsCopy);
	cudaFree(d_fitParameters);
	cudaFree(d_fftFitParameters);
	cudaFree(d_sumFitParams);
	cudaFree(d_relevantFitParams);
	if(weighting == 2){
		cudaFree(d_relevantWaveformPieces);
		cudaFree(d_temporaryYTYValues);
	}
	cudaFree(d_relevantYTY);
	cudaFree(d_tempxATAx);
	cudaFree(d_xATAx);
	cudaFree(d_waveformSquared);
	cudaFree(d_fftWaveformSquared);
	cudaFree(d_chiSquared);
	cudaFree(d_fitChiSquared);
	cudaFree(d_fitResults);
	cudaFreeHost(minChiSquared);
	cudaFree(d_minChiSquared);
	
	if(numIdealPulses>1){
		free(psdPredictions);
		cudaFree(d_psdPredictions);
		free(percents);
		cudaFree(d_percents);

		cudaFree(d_relevantFitPieces);
		if(psdWeight > 0){
			cudaFree(d_psdTempYTY);
		}
		cudaFree(d_psdYTY);
		cudaFree(d_psdTempATA);
		cudaFree(d_psdATA);
		cudaFree(d_PSDChis);
		cudaFree(d_psdParams);
	}
	cufftDestroy(planWavesForward);
	cufftDestroy(planFitParamsInverse);
	cufftDestroy(planSquaredInverse);
	
	//destroy the stream itself
	cudaStreamDestroy(stream);

	//free out the information from compressed waveforms
	cudaFreeHost(compwaves);
	cudaFreeHost(firsts);
	cudaFreeHost(complengths);
	cudaFree(d_firsts);
	cudaFree(d_compwaves);
	cudaFree(d_complengths);
	checkCudaErrors(cudaDeviceSynchronize());
}

}  // Namespace gpuAnalysis
